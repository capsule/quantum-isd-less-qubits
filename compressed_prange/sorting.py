# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Implementation of a quantum sorting network and a 'controlled permutation' 
circuit that permutes an input list according to a switching network.

For both we use the Batcher odd-even mergesort.
(Batcher, K.E.: Sorting networks and their applications. In: AFIPS Spring 
Joint Computing Conference, 1968).

The sorting network is defined by a sequence of comparators and 'switches' which
will swap the entries in place depending on the comparison result. Making the
network reversible requires to store the results of all switches.

"""

import random
from qiskit import QuantumCircuit, QuantumRegister
from math import ceil, log2

from .quantum_util.util import int_to_bits, simulate
from .quantum_util.util import GateCounts
from .quantum_util.basic_arithmetic import HalfAdder
from .abstract_functions import abstract_ceil, abstract_floor, abstract_log2


def batcher_switch_sequence(n):
    """
    Returns the sequence of switches of an instance of Batcher's sorting network,
    as pairs indicating their positions.

    This is an implementation of Algorithm 5.2.2M in Knuth's 'Searching and Sorting'.
    We found it in Perl's Algorithm:Networksort module and re-implemented it in Python.
    It works for any n, even if it's not a power of 2.

    Parameters:
        n - size of the input list
    """
    out = []
    t = ceil(log2(n))
    p = 1 << (t-1)
    while p > 0:
        q = 1 << (t-1)
        r = 0
        d = p
        while d > 0:
            for i in range(n-d):
                if (i & p == r):
                    out.append((i, i+d))
            d = q-p
            q >>= 1
            r = p
        p >>= 1
    return out


class IntegerComparator(QuantumCircuit):
    """
    A comparator between two integers. On input (x,y), it returns 0 if x >= y
    and 1 if x < y. Internally it uses a modified addition circuit.

    Layout: x - y - output (single bit) - ancilla
    """

    def all_counts(bit_size):
        # all obtained from the internal adder circuit
        gcs = GateCounts(ccx=2*bit_size, cx=4*bit_size+1, x=2*bit_size)
        return 5*bit_size + 4, 2*bit_size+2, gcs

    def __init__(self, bit_size):
        super().__init__(name="comp_" + str(bit_size))

        self.bit_size = bit_size
        adder = HalfAdder(bit_size=bit_size, controlled=False,
                          special="comparator")
        self.ancilla_nbr = len(adder.qubits) - 1 - 2*self.bit_size

        qubits = QuantumRegister(len(adder.qubits))
        self.add_register(qubits)
        # complement x
        x_reg = qubits[:bit_size]
        for i in range(len(x_reg)):
            self.x(x_reg[i])
        self.append(adder, qubits)
        for i in range(len(x_reg)):
            self.x(x_reg[i])

    def test(self):
        for _ in range(20):
            x = random.randrange(1 << self.bit_size)
            y = random.randrange(1 << self.bit_size)
            input_bits = (int_to_bits(x, width=self.bit_size) + int_to_bits(y, width=self.bit_size)
                          + [0] + [0] * self.ancilla_nbr)
            output_bits = simulate(self, input_bits)
            expected_output = (int_to_bits(x, width=self.bit_size) + int_to_bits(y, width=self.bit_size)
                               + [int(x < y)] + [0] * self.ancilla_nbr)
            assert output_bits == expected_output


class Switch(QuantumCircuit):
    """A comparator followed by controlled swap.
    If x < y, the comparator returns 1 and we swap, so we always bring the biggest
    integer first.
    This controlled swap is done without fan-out because we care more about
    optimizing the space here than the depth.

    Layout: x - y - output (single bit) - ancilla
    """

    def all_counts(bit_size):
        # counts are the same as a comparator, + the cost of controlled swaps
        gcs = GateCounts(ccx=3*bit_size, cx=6*bit_size+1, x=2*bit_size)
        return 6*bit_size + 4 + 2, 2*bit_size+2, gcs

    def __init__(self, bit_size):
        super().__init__(name="switch_" + str(bit_size))

        self.bit_size = bit_size
        comp = IntegerComparator(bit_size=bit_size)
        self.ancilla_nbr = len(comp.qubits) - 1 - 2*self.bit_size

        qubits = QuantumRegister(len(comp.qubits))
        self.add_register(qubits)
        x_reg = qubits[:bit_size]
        y_reg = qubits[bit_size:(2*bit_size)]
        c_reg = qubits[2*bit_size]
        self.append(comp, qubits)
        # then controlled swap
        for i in range(len(x_reg)):
            # control swap x and y depending on c_reg
            self.cx(x_reg[i], y_reg[i])
            self.ccx(c_reg, y_reg[i], x_reg[i])
            self.cx(x_reg[i], y_reg[i])

    def test(self):
        for _ in range(20):
            x = random.randrange(1 << self.bit_size)
            y = random.randrange(1 << self.bit_size)
            xbits = int_to_bits(x, width=self.bit_size)
            ybits = int_to_bits(y, width=self.bit_size)
            input_bits = (xbits + ybits + [0] + [0] * self.ancilla_nbr)
            output_bits = simulate(self, input_bits)
            if x < y:
                expected_output = (
                    ybits + xbits + [1] + [0] * self.ancilla_nbr)
            else:
                expected_output = (
                    xbits + ybits + [0] + [0] * self.ancilla_nbr)
            assert output_bits == expected_output


class ControlledPermutation(QuantumCircuit):
    """
    A controlled permutation of a bit-vector.
    The input is a register of n bits, and a sequence of enough bits to represent
    the state of switches in a Batcher odd-even mergesort network. The bits are
    swapped in place controlled on the state of the switches.

    Layout: controls (the number is the number of switches in the sorting network)
            - input (n bits)
    """

    def all_counts(n):
        # it depends on the nbr of switches
        logn = abstract_ceil(abstract_log2(n))
        depth = ((logn*(logn+1)//2) * 3 if type(logn)
                 is int else (logn*(logn+1)/2) * 3)
        nb_switches = (n//4 * logn *
                       (logn-1) + n - 1 if type(logn) is int else n /
                       4 * logn * (logn-1) + n - 1)
        gcs = GateCounts(ccx=nb_switches, cx=2*nb_switches)
        return depth, nb_switches + n, gcs

    def __init__(self, n):
        """
        Args:
            n -- size of input vector
        """
        super().__init__(name="controlled_permutation_" + str(n))
        self.n = n
        self.switch_seq = batcher_switch_sequence(self.n)

        controls = QuantumRegister(len(self.switch_seq))
        input_reg = QuantumRegister(self.n)
        self.add_register(controls, input_reg)

        for i, (i1, i2) in enumerate(self.switch_seq):
            # controlled swap i1 and i2 depending on i-th control bit
            self.cx(input_reg[i1], input_reg[i2])
            self.ccx(controls[i], input_reg[i2], input_reg[i1])
            self.cx(input_reg[i1], input_reg[i2])
        # done


class BatcherSort(QuantumCircuit):
    """A reversible variant of Batcher's odd-even mergesort. It applies the
    sequence of switches defined by the function "batcher_switch_sequence" and
    stores the results of the comparators in sufficiently many output bits.

    The input is a list of n integers of bit-size d (hence n*d bits), and sufficiently
    enough space to write the state of the switches.

    Layout: cells - switch outputs - ancillas
    """

    def all_counts(n, d):
        d_s, q_s, g_s = Switch.all_counts(d)

        # upper bound when n is not a power of 2
        logn = abstract_ceil(abstract_log2(n))
        depth_switches = abstract_floor(logn * (logn+1)/2)
        nb_switches = abstract_floor(n/4 * logn * (logn-1)) + n-1

        gcs = g_s * nb_switches
        return d_s * depth_switches, d*n + n + nb_switches, gcs

    def __init__(self, n, d):
        """Build the circuit.

        Args:
            n -- number of integers
            d -- bit-size of integers
        """
        super().__init__(name="batcher_sort_" + str(n) + "_" + str(d))

        # compute sequence of comparators: list of initial numbers is indexed
        # from 0 to n-1
        self.n = n
        self.d = d
        self.switch_seq = batcher_switch_sequence(self.n)

        _switch = Switch(d)

        cells = [QuantumRegister(d) for i in range(self.n)]
        ancillas = QuantumRegister(n)
        switch_outputs = QuantumRegister(len(self.switch_seq))

        self.add_register(*cells)
        self.add_register(switch_outputs)
        self.add_register(ancillas)

        # perform series of switches, grab ancilla when necessary
        # we'll sort in ascending order
        for i, (i1, i2) in enumerate(self.switch_seq):
            self.append(_switch, cells[i2][:] + cells[i1][:]
                        + [switch_outputs[i]] + [ancillas[i1]])

    def test(self):
        for _ in range(4):
            input_nbrs = [random.randrange(1 << self.d) for _ in range(self.n)]
            sorted_nbrs = sorted(input_nbrs)

            input_bits = sum([int_to_bits(t, width=self.d)
                             for t in input_nbrs], [])
            sorted_bits = sum([int_to_bits(t, width=self.d)
                              for t in sorted_nbrs], [])

            complete_inp = input_bits + [0]*(len(self.qubits) - self.n*self.d)
            output_bits = simulate(self, complete_inp)

            assert output_bits[:(self.n*self.d)] == sorted_bits
