# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
A quantum circuit implementation of the Berlekamp-Massey algorithm. See the paper
for more details.
"""

import random
from qiskit import QuantumCircuit, QuantumRegister
from random import randrange
from math import floor

from .quantum_util.util import GateCounts, simulate
from .list_operations import Fanin, Fanout, ListReverter, ShiftRight
from .classical_algorithm import berlekamp_massey

from .abstract_functions import abstract_floor, abstract_max, abstract_min


class ControlledNegateReverse(QuantumCircuit):
    """On unary represented numbers of length n, applies the operation: x -> k+1-x  
    Assuming that input x <= k+1. This sub-circuit is used at each iterate
    of Berlekamp-Massey.
    """

    def all_counts(n, k):
        # there is a fanout of size max( min(k,n), (k)//2)
        kdiv2 = abstract_floor(k / 2)
        d_f, _, g_f = Fanout.all_counts(abstract_max(abstract_min(k, n), kdiv2))
        qubits = n + 1 + abstract_max(abstract_min(k, n), kdiv2)

        gcs = GateCounts(cx=abstract_min(k, n) + 2 * (kdiv2), ccx=kdiv2) + g_f * 2
        depth = 4 + 2 * d_f
        return depth, qubits, gcs


    def __init__(self, n, k):
        """Constructs the circuit. The layout is:
        control (1 bit) | input number (n bits) | ancilla register

        Args:
            n -- length of the unary number
            k -- such that the operation x -> k+1-x  is applied
        """
        super().__init__(name="controlled_negate_reverse_" + str(n) + "_" +
                         str(k))
        control = QuantumRegister(1)
        input_reg = QuantumRegister(n)

        self.n = n
        self.k = k
        self.ancilla_nbr = max(min(k, n), (k) // 2)
        control_fanout = QuantumRegister(self.ancilla_nbr)

        fanout = Fanout(self.ancilla_nbr)

        self.add_register(control, input_reg, control_fanout)
        # fanout the control
        self.append(fanout, [control] + control_fanout[:])
        # negate the bits of input_reg (with the control)
        for i in range(min(k, n)):
            self.cx(control_fanout[i], input_reg[i])
        # control-swap the bits between LSBs and MSBs
        for i in range((k) // 2):
            ind1, ind2 = i, k - 1 - i
            self.cx(input_reg[ind1], input_reg[ind2])
            self.ccx(control_fanout[i], input_reg[ind2], input_reg[ind1])
            self.cx(input_reg[ind1], input_reg[ind2])

        # erase fanout
        self.append(fanout.inverse(), [control] + control_fanout[:])

    def test(self):
        for _ in range(20):
            x = randrange(self.k)
            input_bits = ([1] + [1] * x + [0] * (self.n - x) + [0] *
                          (self.ancilla_nbr))
            expected_output = ([1] + [1] * (self.k - x) + [0] *
                               (self.n - self.k + x) + [0] *
                               (self.ancilla_nbr))
            output_bits = simulate(self, input_bits)
            assert expected_output == output_bits


class ForwardBerlekampMassey(QuantumCircuit):
    """Half of a quantum circuit that implements the Berlekamp-Massey algorithm.

    The input is a list of n bits and a bound d on the expected degree of
    the output. The output is a polynomial of degree <= d-1 represented by its
    list of coefficients.

    Layout: input | C (output) | other registers needing uncomputation
    """

    def all_counts(n, d):
        d_nr, _, g_nr = ControlledNegateReverse.all_counts(n, n)

        # use also fanin and fanout on d bits
        d_fin, _, g_fin = Fanin.all_counts(d, release_input=True)
        d_fout, _, g_fout = Fanout.all_counts(d)

        gcs = g_nr * n + g_fin * (2 * d) + g_fout * (2 * d)
        gcs['x'] += 2 + 2 * n
        gcs['cx'] += n * (2 + g_nr['cx'])
        gcs['ccx'] += n * (3 * d + d + 2 + d) + n * g_nr['ccx']
        gcs['swap'] += n * g_nr['swap'] + n * d

        depth = n * 13 + 2 + n * (2 * d_fin + 2 * d_fout + d_nr)
        qubits = n + 2 * d + n + 2 * d + abstract_max(n - 2 * d + 1, 0) + 2 * n

        return depth, qubits, gcs


    def __init__(self, n, d):
        """Build the circuit.
        
        Args:
            n -- length of input sequence
            d -- bound on the degree of the output polynomial
        """

        super().__init__(name="fwd_berlekamp_massey_" + str(n) + "_" + str(d))
        self.n = n
        self.d = d

        input_reg = QuantumRegister(n)
        C_reg = QuantumRegister(d)
        B_reg = QuantumRegister(d)
        L_reg = QuantumRegister(n)
        # L is represented as a unary vector: L_reg[i] =1 <=> L >= i+1

        # more ancillas for fanin / fanout operations
        ancilla_tof = QuantumRegister(d)
        ancilla_fanin = QuantumRegister(d + max(n - 2 * d + 1, 0))
        ancilla_reverse = ancilla_tof[1:] + ancilla_fanin[:]

        d_reg = QuantumRegister(n)
        v_reg = QuantumRegister(n)

        self.add_register(input_reg, C_reg, B_reg, L_reg, d_reg, v_reg,
                          ancilla_tof, ancilla_fanin)

        # sub-circuits
        shift_right = ShiftRight(d, 1)
        fanin = Fanin(d, release_input=True)
        fanout = Fanout(d - 1)

        # ========= fwd computation

        self.x(C_reg[0])
        self.x(B_reg[0])

        for k in range(self.n):
            # compute new value of d in d_reg[k]
            self.cx(input_reg[k], d_reg[k])

            for i in range(1, self.d):
                # start at i = 1 until i = L included. So we control by the values
                # in L_reg: L_reg[0] = 1 if L >= 1, L_reg[1] if L >= 2, etc
                self.ccx(L_reg[i - 1], input_reg[k - i], ancilla_tof[i - 1])
                self.ccx(C_reg[i], ancilla_tof[i - 1], ancilla_fanin[i - 1])
            self.append(fanin,
                        ancilla_fanin[:d] + [d_reg[k]])  # also input released
            for i in range(1, self.d):
                # start at i = 1 until i = L included. So we control by the values
                # in L_reg: L_reg[0] = 1 if L >= 1, L_reg[1] if L >= 2, etc
                self.ccx(C_reg[i], ancilla_tof[i - 1], ancilla_fanin[i - 1])
                self.ccx(L_reg[i - 1], input_reg[k - i], ancilla_tof[i - 1])
            # here d_reg[k] is computed and ancillas are free

            # vk = int( 2*L <= k )
            # 2*L <= k <=> L <= k/2. <=> L < floor(k/2) + 1 <=> not L_reg[ floor(k/2) ]
            self.cx(L_reg[floor(k / 2.)], v_reg[k])
            self.x(v_reg[k])

            # perform fixed shift
            self.append(shift_right, B_reg[:])

            # controlled on d_reg[k] == 1 do C <- C + B
            # fanout d_reg first
            self.cx(d_reg[k], ancilla_fanin[0])
            self.append(fanout, ancilla_fanin[:d])

            for i in range(self.d):
                self.ccx(ancilla_fanin[i], B_reg[i], C_reg[i])

            self.append(fanout.inverse(), ancilla_fanin[:d])
            self.cx(d_reg[k], ancilla_fanin[0])

            # ===== compute v_reg[k]*d_reg[k] in ancilla_tof[0]
            self.ccx(v_reg[k], d_reg[k], ancilla_tof[0])
            # fanout ancilla_tof
            # first qubit is input
            self.append(fanout, ancilla_tof[:])

            for i in range(self.d):
                self.ccx(ancilla_tof[i], C_reg[i], B_reg[i])
            self.append(fanout.inverse(), ancilla_tof[:])

            # ancillas are freed

            # => update L - but only if dk vk = 1
            # something to gain here: we shouldn't need to re-fanout ancilla_tof[0]
            # since it's already been done.
            _tmp_circuit = ControlledNegateReverse(n, k + 1)
            self.append(_tmp_circuit, [ancilla_tof[0]] + L_reg[:] +
                        ancilla_reverse[:_tmp_circuit.ancilla_nbr])
            # uncompute dk vk
            self.ccx(v_reg[k], d_reg[k], ancilla_tof[0])
            # ======

        # result is in C, but needs yet to be reversed
        # other registers need to be uncomputed



class BerlekampMassey(QuantumCircuit):
    """A full implementation of Berlekamp-Massey.

    Layout: input - output - ancilla
    """

    def all_counts(n, d):
        # this is an upper bound, it's quite precise (a factor 2 or less)
        d_f, q_f, g_f = ForwardBerlekampMassey.all_counts(n, d)

        gcs = g_f * 2
        gcs['cx'] = gcs['cx'] + d

        d_lr, q_lr, g_lr = ListReverter.all_counts(d)
        # take the max between fwd_bm ancillas and reverter ancillas
        qubits = n + d + d + abstract_max(q_f - n - d, q_lr - d)

        gcs['cx'] += g_lr['cx']
        gcs['ccx'] += g_lr['ccx']
        gcs['x'] += g_lr['x']
        gcs['swap'] += g_lr['swap']

        return 2 * d_f + 1 + d_lr, qubits, gcs

    def __init__(self, n, d):
        """
        Build the circuit.

        Args:
            n -- length of the input sequence
            d -- (upper bound on the) length of the output polynomial (its degree
                is actually d-1)
        """
        super().__init__(name="berlekamp_massey_" + str(n) + "_" + str(d))
        self.n = n  # length of input sequence
        self.d = d  # max degree of polynomial wanted

        input_reg = QuantumRegister(n)
        output_reg = QuantumRegister(d)
        tmp_output = QuantumRegister(d)

        fwd_bm = ForwardBerlekampMassey(n, d)
        fwd_bm_ancilla_nbr = len(
            fwd_bm.qubits) - n - d  # output in first d qubits

        reverter = ListReverter(d)
        reverter_ancilla_nbr = reverter.ancilla_nbr

        ancilla_reg = QuantumRegister(
            max(fwd_bm_ancilla_nbr, reverter_ancilla_nbr))

        self.add_register(input_reg, output_reg, tmp_output, ancilla_reg)
        self.ancilla_nbr = len(self.qubits) - n - d

        # start by fwd BM
        self.append(
            fwd_bm,
            input_reg[:] + tmp_output[:] + ancilla_reg[:fwd_bm_ancilla_nbr])
        # copy output
        for i in range(self.d):
            self.cx(tmp_output[i], output_reg[i])
        # do inverse
        self.append(
            fwd_bm.inverse(),
            input_reg[:] + tmp_output[:] + ancilla_reg[:fwd_bm_ancilla_nbr])

        # do the reversion
        self.append(reverter,
                    output_reg[:] + ancilla_reg[:reverter_ancilla_nbr])
        # result is in "output_reg"


    def test(self):
        # compute linear recurrent sequence of order <= d
        l = self.d-2
        rec = [random.randrange(2) for _ in range(l)]
        seq = [random.randrange(2) for _ in range(l)]
        while len(seq) < self.n:
            seq.append( sum([seq[-i-1]*rec[i] for i in range(l) ]) % 2 )
        # sequence is now of length n
        # compute classically the result
        classical_result = berlekamp_massey(seq)
        # simulate circuit
        out = simulate(self, seq + [0] * (len(self.qubits) - self.n))
        # check output
        assert out[self.n:(self.n + self.d)] == classical_result
        assert out[:self.n] == seq
        assert sum(out[(self.n + self.d):]) == 0

