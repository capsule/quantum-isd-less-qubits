# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Entire quantum circuit for Wiedemann's matrix inversion, where the input matrix H_I
is defined by a choice of columns I from the parity-check matrix H. These classes
assume the definition of MultiplicationCircuit (see file multiplication.py).

The output is guaranteed to be good (with some constant probability) only
in the case where H_I is invertible. It may still work in some cases when H_I is not
invertible, but in that case we won't be able to retrieve all solutions.

Each class defines a function 'all_counts' that gives a simplified upper bound
for the gate counts, depth and space of an instance, which can be computed without
creating the entire circuit.

"""

from scipy.linalg import det
from qiskit import QuantumCircuit, QuantumRegister
from random import randrange
from math import log2

from .quantum_util.basic_arithmetic import MCX
from .quantum_util.util import simulate, GateCounts

from .list_operations import Fanout
from .classical_algorithm import M_matrix, evaluate_poly, evaluate_sequence
from .classical_algorithm import sub_matrix, matrix_vector_product
from .berlekamp_massey import BerlekampMassey
from .multiplication import get_multiplication_circuit, get_multiplication_flag
from .multiplication import ConstantMatrixVectorProduct, SPACE_OPTIMIZED

from .abstract_functions import abstract_max, abstract_ceil, abstract_log2


class PolynomialEvaluation(QuantumCircuit):
    """Quantum circuit that evaliates C(HI)*t. 

    Layout: columns, polynomial, input register, output register, ancillas.
    The result is XORed into the output register.
    """

    def all_counts(n, k, d, abstract=False):
        """If 'abstract' is true, will introduce the costs of the 
        multiplication function as variables.  """
        d1, q1, g1 = get_multiplication_circuit().all_counts(n, k, abstract=abstract)
        df, qf, gf = Fanout.all_counts(n - k)
        q = q1 + 2 * (n - k) + 2 * d

        m_cnots = d**2 // 2 if type(d) == int else d**2 / 2
        # estimate of the max number of ones we can have in the connection matrix

        additional_ccx = d * (n - k)
        additional_cnots = 2 * d * (n - k)  # cnots for fanouts
        gcs = GateCounts(cx=2 * m_cnots + additional_cnots, ccx=additional_ccx)
        gcs += g1 * (2 * d)

        # depth of matrix-vector product (optimized with ConstantMatrixVectorProduct) is d
        # depth of fanout and inverse is df
        depth = d * (1 + 2 * df + 2 * d1)
        return depth, q, gcs

    def ancilla_count(n, k, d, abstract=False):
        return d + 2 * (n - k) + get_multiplication_circuit().ancilla_count(
            n, k, abstract=abstract)

    def __init__(self, H, d):
        """
        Parameters:
            H : matrix of dimension n * n-k (as a list of lists, H[i][j] is the element (i,j))
            columns : choice of columns of H (I) that defines the sub-matrix

            d: the number of coefficients of the polynomial. The degree will be d-1.
            (So d matches the number of qubits in the register corresponding to
            the polynomial).
        """

        super().__init__(name="polynomial_evaluation")
        self.H = H
        self.n = len(H[0])
        self.k = self.n - len(H)
        self.d = d

        I_reg = QuantumRegister(get_multiplication_circuit().I_size(self.n))
        c_reg = QuantumRegister(self.d)
        x_reg = QuantumRegister(self.n - self.k)

        out_reg = QuantumRegister(self.n - self.k)
        y_reg = QuantumRegister(self.n - self.k)
        mc_reg = QuantumRegister(self.d)
        mc_fanout = QuantumRegister(self.n - self.k)
        fanout = Fanout(self.n - self.k)

        self.add_register(I_reg, c_reg, x_reg, out_reg, y_reg, mc_reg,
                          mc_fanout)

        multiplication_circuit = get_multiplication_circuit()(self.H)
        ancilla_reg = QuantumRegister(multiplication_circuit.ancilla_nbr)
        self.add_register(ancilla_reg)

        self.ancilla_nbr = (multiplication_circuit.ancilla_nbr + self.d + 2 *
                            (self.n - self.k))

        M = M_matrix(self.d)  # connection matrix
        M_product = ConstantMatrixVectorProduct(M, transposed=False)
        # compute M * c_reg (out of place)
        self.append(M_product, c_reg[:] + mc_reg[:])

        # the output is sum (mc[j] P_j(H_I) t)
        # we start by initializing our registers x,y to t,0
        # (x is properly initialized since it's the input reg itself)
        # at each new multiplication, we obtain P_i(H_I)t, P_(i-1)(H_I)t
        for i in range(self.d):
            # fanout mc_reg[i] into mc_fanout
            self.append(fanout, [mc_reg[i]] + mc_fanout[:])
            # use the qubits of mc_fanout to control the XOR of x to output
            for j in range(self.n - self.k):
                self.ccx(mc_fanout[j], x_reg[j], out_reg[j])
            # erase fanout
            self.append(fanout.inverse(), [mc_reg[i]] + mc_fanout[:])
            # update
            self.append(multiplication_circuit,
                        I_reg[:] + x_reg[:] + y_reg[:] + ancilla_reg[:])

        # output register contains C(HI) t
        # reverse multiplications
        for i in range(self.d):
            self.append(multiplication_circuit.inverse(),
                        I_reg[:] + x_reg[:] + y_reg[:] + ancilla_reg[:])

        # uncompute M * c_reg
        self.append(M_product.inverse(), c_reg[:] + mc_reg[:])

    def test(self):
        for _ in range(20):
            I_val, columns = get_multiplication_circuit().random_I(self.n, self.k)
            x = [randrange(2) for _ in range(self.n - self.k)]
            pol = [randrange(2) for _ in range(self.d)]

            inp = (I_val + pol + x + [0] * (self.n - self.k) + [0] *
                   (self.ancilla_nbr))
            out = simulate(self, inp)
            expected_output = (
                I_val + pol + x +
                evaluate_poly(self.H, columns, x, pol, remove_first=False) +
                [0] * (self.ancilla_nbr))
            # print(expected_output)
            # print(out)
            assert expected_output == out


class SequenceEvaluation(QuantumCircuit):
    """Quantum circuit that evaluates a sequence u^T H_I^i t where u^T selects 
    a single coordinate and H_I is the sub-matrix depending on a column selection I.

    Layout: comumns - input - output (sequence) - ancilla
    """

    def all_counts(n, k, m, abstract=False):
        d1, q1, g1 = get_multiplication_circuit().all_counts(n, k, abstract=abstract)
        df, qf, gf = Fanout.all_counts(m)
        m_cnots = m**2 // 2 if type(m) == int else m**2 / 2
        # estimate of the max number of ones we can have in the connection matrix

        q = q1 + m + m
        d = m + 2 * m * df + d1 * (2 * m)
        gcs = GateCounts(cx=m_cnots)
        gcs += gf * (2 * m)
        gcs += g1 * (2 * m)
        return d, q, gcs

    def ancilla_count(n, k, m, abstract=False):
        return get_multiplication_circuit().ancilla_count(n, k, abstract=abstract) + n - k + m

    def __init__(self, H, i, m):
        """
        Parameters:
            H - matrix
            i - choice of the coordinate to select
            m - length of sequence
        """
        super().__init__(name="sequence_evaluation")
        self.n = len(H[0])
        self.k = self.n - len(H)
        self.H = H
        self.m = m
        self.i = i

        I_reg = QuantumRegister(get_multiplication_circuit().I_size(self.n))
        x_reg = QuantumRegister(self.n - self.k)  # input vector

        out_reg = QuantumRegister(self.m)
        y_reg = QuantumRegister(self.n - self.k)
        x_fanout = QuantumRegister(self.m)

        self.add_register(I_reg, x_reg, out_reg, y_reg, x_fanout)

        fanout = Fanout(self.m)

        multiplication_circuit = get_multiplication_circuit()(self.H)
        ancilla_reg = QuantumRegister(multiplication_circuit.ancilla_nbr)
        self.add_register(ancilla_reg)

        self.ancilla_nbr = multiplication_circuit.ancilla_nbr + self.n - self.k + self.m
        M = M_matrix(self.m)  # connection matrix

        for j in range(m):
            # compute u^T P_i(H_I) t
            # fanout x_reg[self.i] to x_fanout
            self.append(fanout, [x_reg[self.i]] + x_fanout[:])
            for l in range(m):
                if M[l][j]:
                    self.cx(x_fanout[l], out_reg[l])
            # uncompute fanout
            self.append(fanout.inverse(), [x_reg[self.i]] + x_fanout[:])
            # update
            self.append(multiplication_circuit,
                        I_reg[:] + x_reg[:] + y_reg[:] + ancilla_reg[:])

        # uncompute multiplications
        for j in reversed(range(m)):
            self.append(multiplication_circuit.inverse(),
                        I_reg[:] + x_reg[:] + y_reg[:] + ancilla_reg[:])

    def test(self):
        for _ in range(20):
            I_val, columns = get_multiplication_circuit().random_I(self.n, self.k)
            x = [randrange(2) for _ in range(self.n - self.k)]

            inp = I_val + x + [0] * (self.m) + [0] * (self.ancilla_nbr)
            out = simulate(self, inp)
            expected_output = (I_val + x + evaluate_sequence(
                self.H, columns, x, self.i, self.m) + [0] * (self.ancilla_nbr))

            assert expected_output == out


class WiedemannInversion(QuantumCircuit):
    """
    A circuit to perform the inversion of H_I on a constant vector s, using
    Wiedemann's algorithm. I is given as input. Note that the definition of I,
    and how many bits it takes, depends on our choice of multiplication circuit, hence
    of the current get_multiplication_circuit() class. However this is also the only sub-circuit
    where I actually intervenes.

    The output is y = H_I^{-1} s and a boolean indicating success. However the
    cost would be almost the same if we performed a test on the solution s instead
    (we would just have to add the cost of this test), as we would do in 
    Grover's search iterate.

    This implementation uses 2 steps to succeed with constant probability.

    Layout: I - success - output vector - ancilla
    """

    def all_counts(n, k, abstract=False):
        dim = n - k

        s1 = 2 * dim + 2
        s2 = 2 * dim + 2  # dim + 1
        d_s1, _, g_s1 = SequenceEvaluation.all_counts(n,
                                                      k,
                                                      s1,
                                                      abstract=abstract)
        d_s2, _, g_s2 = SequenceEvaluation.all_counts(n,
                                                      k,
                                                      s2,
                                                      abstract=abstract)
        a_s1 = SequenceEvaluation.ancilla_count(n, k, s1, abstract=abstract)
        a_s2 = SequenceEvaluation.ancilla_count(n, k, s2, abstract=abstract)

        d1 = dim + 1
        d2 = dim + 1  # ceil((dim + 1) / 2)

        d_p1, _, g_p1 = PolynomialEvaluation.all_counts(n,
                                                        k,
                                                        d1 - 1,
                                                        abstract=abstract)
        d_p2, _, g_p2 = PolynomialEvaluation.all_counts(n,
                                                        k,
                                                        d2 - 1,
                                                        abstract=abstract)
        a_p1 = PolynomialEvaluation.ancilla_count(n,
                                                  k,
                                                  d1 - 1,
                                                  abstract=abstract)
        a_p2 = PolynomialEvaluation.ancilla_count(n,
                                                  k,
                                                  d2 - 1,
                                                  abstract=abstract)

        d_b1, q_b1, g_b1 = BerlekampMassey.all_counts(s1, d1)
        a_b1 = q_b1 - s1 - d1
        d_b2, q_b2, g_b2 = BerlekampMassey.all_counts(s2, d2)
        a_b2 = q_b2 - s2 - d2

        a_rf = get_multiplication_circuit().ancilla_count(n, k, abstract=abstract)
        d_rf, q_rf, g_rf = get_multiplication_circuit().all_counts(
            n, k, abstract=abstract)
        i_size = q_rf - a_rf - 2 * (n - k)

        # ancilla = max(a_s1, a_s2, a_p1, a_p2, a_rf, a_b1, a_b2)
        # in asymptotic mode we will simplify this by considering that a_b1 is larger
        ancilla = abstract_max(a_b1, a_p1, a_s1)  # for simplification

        q = i_size + 1 + dim * 3 + s1 + s2 + d1 + d2 + ancilla

        gcs = GateCounts(x=2 * dim + 2 * dim, ccx=2 * dim)  # counting MCX cost
        gcs += g_s1 * 2
        gcs += g_s2 * 2
        gcs += g_rf * 4
        gcs += g_p1 * 2
        gcs += g_p2 * 2
        gcs += g_b1 * 2
        gcs += g_b2 * 2

        # MCX cost was omitted here
        depth = (3 + 2 * d_s1 + 2 * d_s2 + 4 * d_rf +
                 2 * d_p1 + 2 * d_p2 + 2 * d_b1 + 2 * d_b2)

        # additional qubit cost for the sorting case. This is because we need
        # storage for the numbers that are sorted during the sampling of a
        # random permutation, even though they are not used afterwards
        # in the multiplication circuits.
        if get_multiplication_flag() != SPACE_OPTIMIZED:
            q += 3 * n * abstract_ceil(abstract_log2(n))

        return depth, q, gcs

    def __init__(self, H, s):
        """
        Parameters:
            H - matrix
            s - input vector
        """
        super().__init__(name="wiedemann_inversion")
        self.n = len(H[0])
        self.k = self.n - len(H)
        self.H = H
        self.s = s
        dim = self.n - self.k  # the dimension we're working with. 'dim-1' is also
        # the maximum degree of the minimal polynomial we're looking for

        I_reg = QuantumRegister(get_multiplication_circuit().I_size(self.n))  # input
        success = QuantumRegister(1)  # first output: bit of success
        output_reg = QuantumRegister(dim)  # second output

        t_reg = QuantumRegister(dim)
        y_reg = QuantumRegister(dim)

        s1 = 2 * dim + 2  # length of first sequence
        s2 = 2 * dim + 2  # dim + 1  # length of second sequence
        seq_1 = QuantumRegister(s1)
        seq_2 = QuantumRegister(s2)

        # we gain some time and space by assuming that the second polynomial
        # is of length <= 1/2 of the first one, when there is one.
        d1 = dim + 1  # degree of first polynomial (upper bound)
        d2 = dim + 1  # ceil((dim + 1) / 2)  # degree of second polynomial
        poly_1 = QuantumRegister(d1)
        poly_2 = QuantumRegister(d2)

        # Recall the layout: columns - input - output (sequence) - ancilla
        seq_eval_1 = SequenceEvaluation(H, 0, s1)
        seq_eval_2 = SequenceEvaluation(H, 1, s2)

        multiplication_circuit = get_multiplication_circuit()(H, do_swap=False)

        # Recall the layout: input - output - ancilla
        bm_1 = BerlekampMassey(s1, d1)
        bm_2 = BerlekampMassey(s2, d2)

        # Recall the layout: columns, polynomial, input register, output register, ancillas
        poly_eval_1 = PolynomialEvaluation(H, d1 - 1)
        poly_eval_2 = PolynomialEvaluation(H, d2 - 1)

        additional_ancilla = max(seq_eval_1.ancilla_nbr,
                                 seq_eval_2.ancilla_nbr, bm_1.ancilla_nbr,
                                 bm_2.ancilla_nbr, poly_eval_1.ancilla_nbr,
                                 poly_eval_2.ancilla_nbr,
                                 multiplication_circuit.ancilla_nbr)
        ancilla_reg = QuantumRegister(additional_ancilla)

        self.add_register(I_reg, success, output_reg, y_reg, t_reg, seq_1,
                          poly_1, seq_2, poly_2, ancilla_reg)
        self.ancilla_nbr = (len(self.qubits) - get_multiplication_circuit().I_size(self.n) -
                            dim - 1)

        # ==== forward computation
        # step 1: write b in t_reg and evaluate the first sequence
        for i in range(dim):
            if s[i]:
                self.x(t_reg[i])

        # evaluate first sequence and apply BM
        self.append(
            seq_eval_1, I_reg[:] + t_reg[:] + seq_1[:] +
            ancilla_reg[:seq_eval_1.ancilla_nbr])
        self.append(bm_1,
                    seq_1[:] + poly_1[:] + ancilla_reg[:bm_1.ancilla_nbr])
        # now poly_1 contains the minimal poly
        # evaluate it on t_reg and XOR result to y

        self.append(
            poly_eval_1, I_reg[:] + poly_1[1:] + t_reg[:] + y_reg[:] +
            ancilla_reg[:poly_eval_1.ancilla_nbr])
        # compute t = t + H_I y via multiplication (without final swap)
        # y, t -> y, t + H_I y
        self.append(
            multiplication_circuit, I_reg[:] + y_reg[:] + t_reg[:] +
            ancilla_reg[:multiplication_circuit.ancilla_nbr])
        # if t = 0 we have already won and y is our solution
        # otherwise we restart from t

        # evaluate sequence and apply BM
        self.append(
            seq_eval_2, I_reg[:] + t_reg[:] + seq_2[:] +
            ancilla_reg[:seq_eval_2.ancilla_nbr])
        self.append(bm_2,
                    seq_2[:] + poly_2[:] + ancilla_reg[:bm_2.ancilla_nbr])

        # evaluate polynomial on input t and XOR result to y
        self.append(
            poly_eval_2, I_reg[:] + poly_2[1:] + t_reg[:] + y_reg[:] +
            ancilla_reg[:poly_eval_2.ancilla_nbr])
        # if t was zero this will XOR 0 to y anyway
        # now y should be our solution

        # copy output
        for i in range(dim):
            self.cx(y_reg[i], output_reg[i])

        # ==== backward computation: we uncompute everything
        self.append(
            poly_eval_2.inverse(), I_reg[:] + poly_2[1:] + t_reg[:] +
            y_reg[:] + ancilla_reg[:poly_eval_2.ancilla_nbr])
        self.append(bm_2.inverse(),
                    seq_2[:] + poly_2[:] + ancilla_reg[:bm_2.ancilla_nbr])
        self.append(
            seq_eval_2.inverse(), I_reg[:] + t_reg[:] + seq_2[:] +
            ancilla_reg[:seq_eval_2.ancilla_nbr])
        self.append(
            multiplication_circuit.inverse(), I_reg[:] + y_reg[:] + t_reg[:] +
            ancilla_reg[:multiplication_circuit.ancilla_nbr])
        self.append(
            poly_eval_1.inverse(), I_reg[:] + poly_1[1:] + t_reg[:] +
            y_reg[:] + ancilla_reg[:poly_eval_1.ancilla_nbr])
        self.append(bm_1.inverse(),
                    seq_1[:] + poly_1[:] + ancilla_reg[:bm_1.ancilla_nbr])
        self.append(
            seq_eval_1.inverse(), I_reg[:] + t_reg[:] + seq_1[:] +
            ancilla_reg[:seq_eval_1.ancilla_nbr])

        # check if HI y = s using a multi-controlled Toffoli
        # here t_reg contains s
        # y, t -> y, t + H_I y
        self.append(
            multiplication_circuit, I_reg[:] + output_reg[:] + t_reg[:] +
            ancilla_reg[:multiplication_circuit.ancilla_nbr])

        for i in range(dim):
            self.x(t_reg[i])

        mcx = MCX(dim)
        self.append(mcx, t_reg[:] + [success] + ancilla_reg[:mcx.ancilla_nbr])

        for i in range(dim):
            self.x(t_reg[i])

        self.append(
            multiplication_circuit.inverse(), I_reg[:] + output_reg[:] + t_reg[:] +
            ancilla_reg[:multiplication_circuit.ancilla_nbr])
        for i in range(dim):
            if s[i]:
                self.x(t_reg[i])

    def test(self, verb=False):
        """If verb is set to True, will also print the results and the
        probability of success. Otherwise it just checks that when there is a
        success, we indeed found the right vector. It also prints a warning 
        if there are no successes. """
        if verb:
            print(self.s)
            print("---")
        succ = 0
        ctr = 0
        trials = 20

        # study only probability of success for invertible matrices
        while ctr < trials:
            I_val, columns = get_multiplication_circuit().random_I(self.n, self.k)
            I_len = get_multiplication_circuit().I_size(self.n)

            HI = sub_matrix(self.H, columns)
            if int(round(det(HI), 1)) % 2 != 0:
                ctr += 1

                inp = I_val + [0] * (self.n - self.k +
                                     1) + [0] * (self.ancilla_nbr)
                out = simulate(self, inp)

                out_succ = out[I_len]
                out_vec = out[(1 + I_len):(1 + I_len + self.n - self.k)]

                assert out[(1 + I_len + self.n -
                            self.k):] == [0] * (self.ancilla_nbr)
                # print the result and see that it works
                if verb:
                    print(matrix_vector_product(HI, out_vec), out_succ)
                if out_succ:
                    assert (matrix_vector_product(HI, out_vec)) == self.s
                if (matrix_vector_product(HI, out_vec)) == self.s:
                    succ += 1
        if succ == 0:
            print("[WARNING] no success after 20 trials, this is highly improbable")
        if verb:
            print(succ)
            print(log2(succ / ctr))


