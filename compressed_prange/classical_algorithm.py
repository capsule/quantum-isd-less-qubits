# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Classical implementation of Wiedemann matrix inversion, intended to match the quantum
implementation (used for testing purposes). The results are the same. This
allows to estimate the success probability when input matrices (i.e., a choice
of columns in the parity-check matrix) are taken at random.

"""

from random import randrange, shuffle
from scipy.linalg import inv, det
from math import ceil, log2


# ========= BASIC FUNCTIONS =====


def matrix_vector_product(A, x):
    """Bololean matrix-vector product when the input matrix is represented as a 
    list of lists of 0-1 values. 

    Parameters:
        A - matrix
        x - vector
    """
    if len(A) != len(x):
        raise ValueError("Matrix and vector dimensions do not match")
    result = []
    for row in A:
        tmp = 0
        for i in range(len(row)):
            tmp ^= row[i] * x[i]
        result.append(tmp)
    return result


def sub_matrix(A, I):
    """Returns a sub-matrix (as a list of lists) obtained by selecting
    a subset of the columns of an input matrix.

    Parameters:
        A - matrix
        I - columns 
    """
    if all([t * (t - 1) == 0 for t in I]):
        # columns in 0-1 representation
        result = []
        for row in A:
            tmp = []
            for i in range(len(row)):
                if I[i]:
                    # keep it
                    tmp.append(row[i])
            result.append(tmp)
        return result
    else:
        # columns in selection representation
        result = [[0 for _ in range(max(I) + 1)] for _ in range(max(I) + 1)]
        for i, ind in enumerate(I):
            if ind >= 0:
                # put col i of A in position ind
                for j in range(max(I) + 1):
                    result[j][ind] = A[j][i]
        return result


def degree(l):
    """Finds the last position of a '1' in a list of 0 and 1."""
    for i in range(len(l) - 1, -1, -1):
        if l[i] == 1:
            return i
    return 0


def reversion(l):
    """'Reverses' a list that represents a polynomial (which starts with a 1)."""
    d = degree(l)
    return list(reversed(l[:(d + 1)])) + [0] * (len(l) - d - 1)


# ========= WIEDEMANN'S ALGORITHM =====


def berlekamp_massey(seq):
    """
    Simplified version of the Berlekamp-Massey algorithm, which differs from
    its description in "Shift-register synthesis and BCH Decoding" (Massey, IEEE
    Trans. Inf. Theory 1969). The structure is intended to match our reversible
    implementation. This implementation is binary only (input is a list of 0-1).
    """

    L = 0
    N = len(seq)
    # upper bound on the degree of the polynomial we're looking for
    max_deg = N // 2
    max_deg_log = ceil(log2(max_deg))
    C = [1] + [0] * (max_deg)
    B = [1] + [0] * (max_deg)
    d = [0] * N  # storage of dk values
    v = [0] * N  # storage of vk values

    for k in range(N):

        d[k] = seq[k]
        for i in range(1, L + 1):
            d[k] ^= (C[i] * seq[k - i])

        v[k] = int(2 * L <= k)

        B = B[-1:] + B[:-1]

        if d[k] == 1:
            for i in range(max_deg + 1):
                C[i] = C[i] ^ B[i]

        if d[k] * v[k] == 1:
            for i in range(max_deg + 1):
                B[i] = B[i] ^ C[i]
            # update L
            L = k + 1 - L

    # at the end we need to reverse the polynomial C
    return reversion(C)


def multiplication(H, columns, x, y):
    """
    Applies the operation I, x,y -> I, y + H_I x, x 
    where H_I is the sub-matrix extracted from H by choosing the columns at positions I.

    *If I is represented as a list of 0-1 only, then the columns are in order
    *If I is a list of integers instead, then the columns are not in order: we 
    are also permuting them.

    Parameters:
        H -- matrix of dimension n * n-k (as a list of lists, H[i][j] is the element (i,j))
        columns -- choice of columns of H (I) that defines the sub-matrix
        x,y -- vectors of dimension n-k
    """
    n = len(H[0])
    k = n - len(H)

    if all([t * (t - 1) == 0 for t in columns]):
        counting_reg = [1] + [0] * (n - k - 1)
        v = [0] * n
        for i in range(n):
            for l in range(n - k):
                v[i] ^= x[l] * counting_reg[l]
            v[i] *= columns[i]
            if columns[i]:
                # shift: move right
                counting_reg = counting_reg[-1:] + counting_reg[:-1]
        # vector v created
        for i in range(n - k):
            for l in range(n):
                y[i] ^= v[l] * H[i][l]
        # then do the swap of outputs
    else:
        v = [(x[i] if i >= 0 else 0) for i in columns]
        for i in range(n - k):
            for l in range(n):
                y[i] ^= v[l] * H[i][l]
    return y, x


def M_matrix(n):
    """
    Returns the n * n lower triangular binary matrix which gives the relation 
    between the polynomials P_i (defined in the paper) and the monomials X^i,
    as: M (P_i) = (X^i).
    """
    M = [[0 for _ in range(n)] for _ in range(n)]
    M[0][0] = 1
    M[1][0] = 0
    M[1][1] = 1
    # construct via recurrence relation of Pi
    for i in range(2, n):
        for j in range(0, n):
            M[i][j] = (M[i][j] + M[i - 2][j]) % 2
        for j in range(1, n):
            M[i][j] = (M[i][j] + M[i - 1][j - 1]) % 2
    # invert
    res = inv(M).tolist()
    return [[int(abs(t)) for t in tmp] for tmp in res]


def evaluate_poly(H, columns, t, pol, remove_first=False):
    """
    Evaluates C(H_I)*t for an input polynomial C, by only accessing H_I through
    the "multiplication" which performs out-of-place matrix multiplication.

    Parameters:
        H - matrix
        columns - choice of columns I
        t - input vector
        pol - polynomial
    """
    coeffts = pol[:]
    if remove_first:
        coeffts = coeffts[1:]

    d = len(coeffts)  # d-1 is the degree of the polynomial
    M = M_matrix(d)  # connection matrix
    # start by computing M * c
    mc = [0] * d
    for i in range(d):
        for j in range(d):
            if M[i][j]:
                mc[j] ^= coeffts[i]

    # the output is sum (mc[j] P_j(H_I) t)
    # we start by initializing our registers x,y to t,0
    # at each new round, we obtain P_i(H_I)t, P_(i-1)(H_I)t
    x, y = t[:], [0] * (len(t))
    res = [0] * (len(t))

    for i in range(d):
        # XOR current x in output with coefficient mc[j]
        for j in range(len(t)):
            res[j] ^= mc[i] * x[j]
        # update
        x, y = multiplication(H, columns, x, y)

    # output should be C(HI) t
    return res


def evaluate_sequence(H, columns, t, i, m):
    """
    Evaluates the sequence of u^T H_I^i t where u^T selects a single coordinate
    and H_I is the sub-matrix depending on a column selection I.

    Parameters :
        H - matrix (list of lists)
        columns - choice of columns I (list)
        t - input vector
        i - choice of coordinate
        m - length of the sequence
    """
    M = M_matrix(m)  # connection matrix
    # at each new round, we obtain P_i(H_I)t, P_(i-1)(H_I)t
    x, y = t[:], [0] * (len(t))
    seq = [0] * m
    for j in range(m):
        # compute u^T P_i(H_I) t
        tmp = x[i]
        # XOR it everywhere we need it
        for l in range(m):
            if M[l][j]:
                seq[l] ^= tmp
        # update
        x, y = multiplication(H, columns, x, y)
    return seq


def wiedemann(H, I, s):
    """
    Applies Wiedemann's inversion algorithm (with two steps) to find y such that
    H_I y = s.

    Parameters:
        H - matrix
        I - selection of columns which defines the sub-matrix H_I (as a list)
        s - column vector (list)
    """
    n = len(H[0])
    k = n - len(H)
    HI = sub_matrix(H, I)

    t = s[:]  # copy of s
    y = [0 for _ in range(n - k)]
    i = 0
    while i < 2:
        # evaluate sequence
        seq = evaluate_sequence(H, I, t[:], i, 2 * (n - k))
        # sequence is computed
        c = berlekamp_massey(seq)
        # compute reduced min pol
        tmp = evaluate_poly(H, I, t[:], c, remove_first=True)
        # update y
        y = [y[j] ^ tmp[j] for j in range(n - k)]
        tmp = matrix_vector_product(HI, y)
        # update t
        t = [s[j] ^ tmp[j] for j in range(n - k)]
        i += 1

    # we'll need to check afterwards that t == 0
    return y


def test_random_matrix(n):
    """
    Test the "wiedemann" function with a random n*n Boolean matrix.
    """
    H = [[(randrange(2)) for _ in range(n)] for _ in range(n)]
    s = [randrange(2) for _ in range(n)]

    y = wiedemann(H, s)
    return matrix_vector_product(H, y) == s


if __name__ == "__main__":

    def test_success_probability():
        # testing the success probability of matrix inversion using the
        # "wiedemann" function
        n = 40
        k = 20
        H = [[randrange(2) for _ in range(n)] for _ in range(n - k)]
        b = [randrange(2) for _ in range(n - k)]

        trials = 100
        columns = [1] * (n - k) + [0] * k
        succ = 0
        ctr = 0
        while ctr < trials:
            # random selection of sub-matrix
            shuffle(columns)
            HI = sub_matrix(H, columns)

            if int(round(det(HI), 1)) % 2 != 0:
                ctr += 1
                y = wiedemann(H, columns, b)
                if (matrix_vector_product(HI, y)) == b:
                    succ += 1
        # number of successes, and probability of success
        print(succ)
        print(log2(succ / ctr))
