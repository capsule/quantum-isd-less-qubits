# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Defines abstract functions for log2, ceil, floor, max and min which can be evaluated.
This allows to switch between formulas and actual numbers when evaluating the
costs of the circuit classes.
"""

import sympy as sp
import math


class abstract_log2(sp.Function):
    @classmethod
    def eval(cls, x):
        if x.is_number:
            return math.log2(x)


class abstract_ceil(sp.Function):
    @classmethod
    def eval(cls, x):
        if x.is_number:
            return int(math.ceil(x))


class abstract_floor(sp.Function):
    @classmethod
    def eval(cls, x):
        if x.is_number:
            return int(math.floor(x))


class abstract_max(sp.Function):
    """
    An abstract 'maximum' function.

    Caution: in asymptotic mode, it is expected that the *first* argument is
    actually the largest term  (we will use this later).
    """
    @classmethod
    def eval(cls, *x):
        if all([t.is_number for t in x]):
            return max(*x)


class abstract_min(sp.Function):
    """
    An abstract 'minimum' function. 

    Caution: in asymptotic mode, it is expected that the *first* argument is
    actually the smallest term (we will use this later).
    """
    @classmethod
    def eval(cls, *x):
        if all([t.is_number for t in x]):
            return min(*x)



