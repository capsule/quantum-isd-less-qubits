# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Quantum circuits which operate on lists of bits. Also contains quantum circuits
for fan-in and fan-out, which are important to reduce the depth of our circuits.
"""

from qiskit import QuantumCircuit, QuantumRegister
from random import randrange
from sympy.combinatorics import Permutation
from math import ceil, log2

from .abstract_functions import abstract_ceil, abstract_log2
from .quantum_util.util import GateCounts, simulate
from .quantum_util.modular_multi_product import MultiBitSum
from .classical_algorithm import reversion


class ShiftRight(QuantumCircuit):
    """A shift to the right. Implements both a non-controlled and a controlled
    in-place version. The controlled in-place version uses a decomposition
    into a product of permutations, and has O(n) depth. (It's not used in 
    our implementations).
    """

    def __init__(self, n, offset, controlled=False):
        super().__init__(name="shift" + str(n) + "_" + str(offset))
        self.n = n
        self.offset = offset
        self.controlled = controlled

        if controlled:
            control = QuantumRegister(1)
            self.add_register(control)

        input_reg = QuantumRegister(n)
        self.add_register(input_reg)

        # transform this right-shift into a list of transpositions
        l = list(range(self.n))
        l_perm = (l[-offset:] + l[:-offset] if offset >= 0 else l[:-offset] +
                  l[-offset:])
        transp = Permutation(l_perm).transpositions()

        # apply swaps or controlled swaps
        for t in transp:
            if controlled:
                self.cx(input_reg[t[0]], input_reg[t[1]])
                self.ccx(control, input_reg[t[1]], input_reg[t[0]])
                self.cx(input_reg[t[0]], input_reg[t[1]])
            else:
                self.swap(t[0], t[1])

    def test(self):
        for _ in range(20):
            if self.controlled:
                l = [randrange(2) for _ in range(self.n)]
                output_bits = simulate(self, l)
                expected_output = (l[-self.offset:] + l[:-self.offset]
                                   if self.offset >= 0 else l[:-self.offset] +
                                   l[-self.offset:])
                assert output_bits == expected_output


class Fanout(QuantumCircuit):
    """A fanout circuit. Maps 00..0 to 00..0 and 10..0 to 11..1. The circuit is
    ancilla-free.

    Layout: input (one bit) - output (n bits)
    """

    def all_counts(n):
        depth = abstract_ceil(abstract_log2(n)) + 1
        qubits = n + 1
        gcs = GateCounts(cx=n)
        return depth, qubits, gcs

    def __init__(self, n):
        super().__init__(name="fanout" + str(n))
        self.n = n

        input_reg = QuantumRegister(1)
        output_reg = QuantumRegister(n)

        self.add_register(input_reg, output_reg)
        self.ancilla_nbr = 0

        # work in place on output_reg (assumed to start at 0)
        max_seen = 1
        # start from 1 "seen" at pos 0
        ops = []
        while max_seen < self.n:
            for i in range(0, max_seen):
                if max_seen + i < self.n:
                    ops.append((i, max_seen + i))
            max_seen = 2 * max_seen
        # print(ops)
        self.cx(input_reg, output_reg[0])

        for a, b in ops:
            self.cx(output_reg[a], output_reg[b])

    def test(self):
        output_bits = simulate(self, [1] + [0] * self.n)
        assert output_bits == [1] * (self.n + 1)
        output_bits = simulate(self, [0] + [0] * self.n)
        assert output_bits == [0] * (self.n + 1)


class Fanin(QuantumCircuit):
    """A fanin circuit. On input (x_0 x_1 .. x_{n-1} b), XORs all x_i to b
    using a binary tree structure. The circuit is ancilla-free.
    """

    def all_counts(n, release_input=True):
        if release_input:
            depth = 2 * (abstract_ceil(abstract_log2(n)) + 1) - 1
            gcs = GateCounts(cx=2 * n - 1)
        else:
            depth = abstract_ceil(abstract_log2(n)) + 1
            gcs = GateCounts(cx=n)
        return depth, n + 1, gcs

    def __init__(self, n, release_input=True):
        """
        Parameters:
            n -- input list size
            release_input -- if set to True, will leave the input unchanged.
                    Otherwise, the input will be modified as CNOTs are performed in place.
        """
        super().__init__(name="fanin" + str(n))
        self.n = n
        self.release_input = release_input

        input_reg = QuantumRegister(n)
        output_reg = QuantumRegister(1)

        self.add_register(input_reg, output_reg)
        self.ancilla_nbr = 0

        # work in place on input_reg
        current_level = [i for i in range(self.n)]
        ops = []
        while len(current_level) > 1:
            # XOR in pairs, then update current_level
            next_level = []
            for i in range(len(current_level) // 2):
                next_level.append(current_level[2 * i + 1])
                ops.append((current_level[2 * i], current_level[2 * i + 1]))
            if len(current_level) % 2 == 1:
                next_level.append(current_level[-1])
            current_level = next_level
        # only 1 elt remains

        for a, b in ops:
            self.cx(a, b)
        self.cx(current_level[0], output_reg)

        if release_input:
            for a, b in reversed(ops):
                self.cx(a, b)

    def test(self):
        for _ in range(20):
            l = [randrange(2) for _ in range(self.n)]
            inp = l[:] + [0]
            expected_output = l[:] + [sum(l) % 2]
            output = simulate(self, inp)
            assert output == expected_output


class ControlledShiftLeft(QuantumCircuit):
    """
    A controlled shift to the left by a given offset.

    In python list syntax, this circuit has the same effect as doing:
        reg[offset:] + reg[:offset] if offset is positive
        reg[:offset] + reg[offset:] if offset is negative

    This circuit is optimized for depth. It uses two ancillary registers: the
    first one is where the output is written using CCNOTs. The second one is
    used for fan-out of the control. The total depth is dominated by the fan-out
    (logarithmic), since other operations are constant-depth.
    """

    def all_counts(n):
        f_depth, f_qubits, f_gcs = Fanout.all_counts(n)
        gcs = GateCounts(ccx=3 * n, cx=2 * n + 2 * f_gcs['cx'])
        depth = 2 * f_depth + 5
        qubits = 3 * n - 1 + 1
        return depth, qubits, gcs

    def __init__(self, n, offset):
        super().__init__(name="controlled_shift" + str(n) + "_" + str(offset))
        self.n = n
        self.offset = offset  # can be negative
        self.ancilla_nbr = n + (n - 1)

        control = QuantumRegister(1)
        input_reg = QuantumRegister(n)
        output_reg = QuantumRegister(n)
        control_fanout = QuantumRegister(n - 1)
        control_reg = [control] + control_fanout[:]

        # fanout is on the last qubits
        self.add_register(control, input_reg, output_reg, control_fanout)
        fanout = Fanout(n - 1)

        self.append(fanout, control_reg)
        for i in range(self.n):
            # XOR input_reg[i] to output_reg[i - offset] (shift left)
            self.ccx(control_reg[i], input_reg[i],
                     output_reg[(i - self.offset + self.n) % self.n])
        for i in range(self.n):
            # erase input_reg
            self.ccx(control_reg[i], output_reg[i],
                     input_reg[(i + self.offset + self.n) % self.n])
        for i in range(self.n):
            # swap input and output, but only if control is 1
            self.cx(input_reg[i], output_reg[i])
            self.ccx(control_reg[i], output_reg[i], input_reg[i])
            self.cx(input_reg[i], output_reg[i])
        self.append(fanout.inverse(), control_reg)

    def test(self):
        for _ in range(20):
            input_bits = [randrange(2) for _ in range(self.n + 1)]
            output_bits = simulate(self, input_bits + [0] * (2 * self.n - 1))
            if input_bits[0] == 1:
                if self.offset < 0:
                    expected_output = [
                        1
                    ] + input_bits[1:][:self.offset] + input_bits[1:][
                        self.offset:] + [0] * (2 * self.n - 1)
                else:
                    expected_output = [
                        1
                    ] + input_bits[1:][self.offset:] + input_bits[
                        1:][:self.offset] + [0] * (2 * self.n - 1)
            else:
                expected_output = input_bits[:] + [0] * (2 * self.n - 1)
            assert output_bits == expected_output


class ListReverter(QuantumCircuit):
    """A circuit that 'reverts' a list in about O(n log n) gates.
    It reverts the order of the list *without* the trailing zeroes, whichever the
    amount of trailing zeroes.

    The circuit only works properly if the input list starts with 1. Otherwise
    the result may be inconsistent, and the ancillas will not properly be 
    put back to 0 (the operation is not reversible if the list does not start
    with 1).

    Currently the depth is not optimized, as it is negligible in our computations
    anyway.
    """

    def all_counts(n):
        # due to the sub-circuits, computing the gate counts here is quite complicated.
        # So instead we give an upper bound (it's enough because this circuit does
        # not dominate at all in our estimates)
        gcs = GateCounts(x=4 * n,
                         ccx=8 * n * abstract_ceil(abstract_log2(n)),
                         cx=10 * n * abstract_ceil(abstract_log2(n)),
                         swap=n // 2 if type(n) == int else n / 2)
        depth = 8 * n
        qubits = 5 * n
        return depth, qubits, gcs

    def __init__(self, n):
        super().__init__(name="reverter_" + str(n))

        self.n = n
        input_reg = QuantumRegister(n)

        # for computation of degree
        tmp_reg = QuantumRegister(n)

        bit_summation_circuit = MultiBitSum(self.n, ceil(log2(self.n)) + 4)
        bit_summation_output_size = bit_summation_circuit.output_size

        deg_reg = QuantumRegister(bit_summation_output_size)
        bit_summation_ancilla_nbr = len(
            bit_summation_circuit.qubits) - n - bit_summation_output_size
        summation_ancilla = QuantumRegister(bit_summation_ancilla_nbr)
        shift_ancilla = QuantumRegister(
            2 * n - 1)  # nbr of ancillas required for controlled shift

        self.add_register(input_reg, tmp_reg, deg_reg, summation_ancilla,
                          shift_ancilla)
        self.ancilla_nbr = len(tmp_reg) + len(deg_reg) + len(
            summation_ancilla) + len(shift_ancilla)

        # == step 1: compute degree and write in deg_reg
        for i in range(self.n):
            self.x(input_reg[i])

        # write 1 in last bit
        self.cx(input_reg[self.n - 1], tmp_reg[self.n - 1])
        for i in range(self.n - 2, -1, -1):
            self.ccx(input_reg[i], tmp_reg[i + 1], tmp_reg[i])

        for i in range(self.n):
            self.x(input_reg[i])

        self.append(
            bit_summation_circuit, tmp_reg[:] + deg_reg[:] +
            summation_ancilla[:bit_summation_ancilla_nbr])
        # now tmp_reg contains junk, and deg_reg contains the degree output
        # reverse the list
        for i in range(0, n // 2):
            self.swap(input_reg[i], input_reg[-i - 1])
        # do a series of controlled shifts

        for i in range(bit_summation_output_size):
            cshift = ControlledShiftLeft(self.n, 2**i)
            self.append(cshift, [deg_reg[i]] + input_reg[:] + shift_ancilla[:])

        # recompute the degree & erase everything
        self.append(
            bit_summation_circuit.inverse(), tmp_reg[:] + deg_reg[:] +
            summation_ancilla[:bit_summation_ancilla_nbr])
        for i in range(self.n):
            self.x(input_reg[i])
        for i in reversed(range(self.n - 2, -1, -1)):
            self.ccx(input_reg[i], tmp_reg[i + 1], tmp_reg[i])
        self.cx(input_reg[self.n - 1], tmp_reg[self.n - 1])

        for i in range(self.n):
            self.x(input_reg[i])
        # done

    def test(self):
        for _ in range(2):
            # guaranteed to start with 1
            input_bits = [1] + [randrange(2) for _ in range(self.n - 1)]
            output_bits = simulate(self, input_bits + [0] * self.ancilla_nbr)
            expected_output = reversion(input_bits) + [0] * self.ancilla_nbr
            assert output_bits == expected_output
