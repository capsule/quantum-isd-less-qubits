# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Implementation(s) of the (matrix-vector) multiplication circuit, where
the matrix is implicitly represented by I.

We support different representations of the choice I. The representation itself
only intervenes in the multiplication. But other quantum circuits need to know
the size of this representation. They access it using class methods of the
Multiplication class.

We support three multiplication circuits:
- SPACE_OPTIMIZED : the space-optimized circuit
- SORTING_BASIC : the toffoli-optimized circuit
- SORTING_KARATSUBA : the gate-optimized circuit

To switch between them, use: 'set_multiplication_flag()' and 'get_multiplication_flag()'
with any of these three flags. The circuit class can then be accessed by:
'get_multiplication_circuit()'. The variables MultiplicationCircuit and
MULTIPLICATION_FLAG store this information globally.

"""

from qiskit import QuantumCircuit, QuantumRegister
from random import randrange, shuffle
from sympy import Symbol

from .quantum_util.util import GateCounts, simulate
from .list_operations import Fanin, Fanout, ControlledShiftLeft
from .classical_algorithm import multiplication
from .sorting import batcher_switch_sequence, ControlledPermutation
from .karatsuba_multiplication import KaratsubaConstantCirculantMatrixVectorProduct
from .abstract_functions import abstract_ceil, abstract_log2


# ==========================
# flag to decide which implementation to use

# We have three choices:
# - Space-optimized: the best in space, uses O(n) qubits only
# - Sorting-Basic: uses asymptotically more qubits due to the use of a switching
# network to represent the choice of columns. The constant matrix-vector product
# is performed naively.
# - Sorting-Karatsuba: same, but the constant matrix-vector product is performed
# via Karatsuba multiplication (it works only if the matrix is circulant).

SPACE_OPTIMIZED = "space_optimized"
SORTING_BASIC = "sorting_basic"
SORTING_KARATSUBA = "sorting_karatsuba"

# default
MULTIPLICATION_FLAG = SORTING_BASIC


# ========================================


def random_columns(n, k):
    """Creates a random list of 0/1 of length n with Hamming weight k."""
    res = [0] * k + [1]*(n-k)
    shuffle(res)
    return res


class ConstantMatrixVectorProduct(QuantumCircuit):
    """
    Multiplies by a constant binary matrix, using only CNOTs (out of place).
    """

    def all_counts(n, k):
        gcs = GateCounts(cx=(n-k)*n)
        depth = n
        q = n + n-k
        return depth, q, gcs

    def __init__(self, H, transposed=True):
        """
        Build the circuit.

        Args:
            H -- matrix (as a list of lists)
            transposed -- determine if we transpose the matrix or not
        """
        super().__init__(name="matrix_product")
        self.n = len(H[0])
        self.k = self.n-len(H)
        self.H = H

        # registers
        x_reg = QuantumRegister(self.n)
        y_reg = QuantumRegister(self.n-self.k)

        self.add_register(x_reg, y_reg)

        # in order to be near-optimal in depth, we order the pairs
        # l, i so that they are disjoint in successive layers
        for i in range(self.n):
            # all of this is done in depth 1, since all the a
            # and all the b are distinct
            for u in range(self.n - self.k):
                a, b = u, (i+u) % self.n
                if (transposed and H[a][b] == 1) or (not transposed and H[b][a] == 1):
                    self.cx(x_reg[b], y_reg[a])


class MultiplicationBasic(QuantumCircuit):
    """
    A circuit that applies the operation I, x,y -> I, y + H_I x, x.
    This class is also responsible for the representation of I, using two functions:
    - "I_size" that specifies its size (in bits) from n bits
    - "random_I" that outputs a random I fitting this description, as well as its
        simple representation as an n-bit list

    This is the space-optimized version of the function, which uses a
    representation of I of size O(n) bits, and uses O(n) bits of space overall.

    Layout of the circuit: I - x - y - ancilla
    """

    def random_I(n, k):
        """Return a random choice of I, specific to this multiplication 
        implementation, as well as its simple representation as an n-bit binary list."""
        out = random_columns(n, k)
        return out, out

    def I_size(n):
        """Return the size of I if the input vector has n bits, specific to 
        this multiplication implementation."""
        return n

    def all_counts(n, k, do_swap=True, abstract=False):
        if abstract:
            return Symbol("D"), Symbol("Q"), GateCounts(cx=Symbol("CX"), ccx=Symbol("CCX"), x=Symbol("X"))
        d1, _, g1 = Fanin.all_counts(n-k, release_input=False)
        d2, _, g2 = Fanout.all_counts(n-k)
        _, _, g3 = ControlledShiftLeft.all_counts(n-k)

        gcs = GateCounts(x=2)
        # upper bound on the number of ones in H
        gcs['cx'] = gcs['cx'] + (n-k)*n
        # (in order to go even faster)
        gcs['ccx'] = gcs['ccx'] + n * (2 + 2*(n-k))
        gcs = gcs + g3*n
        gcs = gcs + g2*(2*n)
        gcs = gcs + g1*(2*n)
        if do_swap:
            gcs['swap'] = n-k

        d = 2 + n*(1 + d1 + 1 + d2 + 1 + d2 + 1 + d1 + 1 + 5) + d2
        # the fanin/fanout of the ctrl_shift_right are parallelized with other
        # operations, except a fanout at the last iterate

        q = 2 + 5*(n-k) + MultiplicationBasic.I_size(n)
        return d, q, gcs

    def ancilla_count(n, k, abstract=False):
        if abstract:
            return Symbol("A")
        return (2 + 5*(n-k)) - (n-k)*2

    def __init__(self, H, do_swap=True):
        """Build the circuit.

        Parameters:
            H -- matrix of dimension n * n-k (as a list of lists, H[i][j] is the element (i,j))
            do_swap -- True if the x and y registers are swapped after the operation.
                    Otherwise y is modified and the registers are not swapped.
        """
        super().__init__(name="multiplication")

        # retrieve dimensions of matrix
        self.n = len(H[0])
        self.k = self.n-len(H)
        self.H = H
        self.do_swap = do_swap

        # registers
        x_reg = QuantumRegister(self.n-self.k)
        y_reg = QuantumRegister(self.n-self.k)
        I_reg = QuantumRegister(MultiplicationBasic.I_size(self.n))
        # unary counter (a single 1)
        counting_reg = QuantumRegister(self.n-self.k)
        fanin_ancilla = QuantumRegister(self.n-self.k)  # ancillas for fanin
        fanout_ancilla = QuantumRegister(self.n-self.k)  # ancillas for fanout
        # place the fanin ancilla last, in order to parallelize some operations
        shift_ancilla = fanin_ancilla[:] + fanout_ancilla[:-1]

        v_reg = QuantumRegister(1)
        ancilla_tof = QuantumRegister(1)

        self.add_register(I_reg, x_reg, y_reg, counting_reg, v_reg,
                          ancilla_tof, fanin_ancilla, fanout_ancilla)
        self.ancilla_nbr = (
            len(self.qubits) - MultiplicationBasic.I_size(self.n) - (self.n-self.k)*2)

        fanin = Fanin(self.n-self.k, release_input=False)
        fanout = Fanout(self.n-self.k)
        ctrl_shift_right = ControlledShiftLeft(self.n-self.k, 1).inverse()

        # ============

        self.x(counting_reg[0])
        for i in range(self.n):
            # compute the value of v
            # compute ancilla_tof = xor (x_reg[l]*counting_reg[l])
            # this has constant depth
            for l in range(self.n-self.k):
                self.ccx(x_reg[l], counting_reg[l], fanin_ancilla[l])
            # fan-in to ancilla_tof -> log depth
            self.append(fanin, fanin_ancilla[:] + [ancilla_tof])
            self.ccx(ancilla_tof, I_reg[i], v_reg)

            # XOR vreg[:] to all fanout_ancilla
            self.append(fanout, v_reg[:] + fanout_ancilla[:])
            for l in range(self.n-self.k):
                if H[l][i]:
                    self.cx(fanout_ancilla[l], y_reg[l])
            self.append(fanout.inverse(), v_reg[:] + fanout_ancilla[:])

            # erase the value of v
            self.ccx(ancilla_tof, I_reg[i], v_reg)
            # uncompute fanin. This has log depth again
            self.append(fanin.inverse(), fanin_ancilla[:] + [ancilla_tof])
            for l in range(self.n-self.k):
                self.ccx(x_reg[l], counting_reg[l], fanin_ancilla[l])

            # update counting_reg (increase counter)
            # needs ancilla, but at this point they have been released
            # ctrl_shift_right requires a fanout, which happens on the last n-1 qubits.
            # so this first fanout can be parallelized with the fanin_inverse() which
            # uses fanin_ancilla
            self.append(ctrl_shift_right, [
                        I_reg[i]] + counting_reg[:] + shift_ancilla[:])

        # at the end, counting_reg is back to 0
        self.x(counting_reg[0])

        # ===========================

        if self.do_swap:
            # then swap x and y
            for i in range(self.n-self.k):
                self.swap(x_reg[i], y_reg[i])

    def test(self):
        for _ in range(20):
            I_val, columns = MultiplicationBasic.random_I(self.n, self.k)
            x = [randrange(2) for _ in range(self.n-self.k)]
            y = [randrange(2) for _ in range(self.n-self.k)]
            inp = I_val + x + y + [0]*(self.ancilla_nbr)
            out = simulate(self, inp)
            a, b = multiplication(self.H, columns, x[:], y[:])
            expected_output = I_val + a + b + [0]*(self.ancilla_nbr)
            assert out == expected_output


# =================================
# sorting-based representation

def sorting_based_random_cols(n, k):
    """Returns a random selection of columns in the sorting-based representation,
    obtained by permuting a list according to a random switching network.
    """
    out = list(range(n-k)) + [-1]*k
    l = batcher_switch_sequence(n)
    switches = [randrange(2) for _ in range(len(l))]

    for i, (i1, i2) in enumerate(l):
        if switches[i]:
            # transpose
            tmp = out[i1]
            out[i1] = out[i2]
            out[i2] = tmp
    return switches, out


# limit below which we use naive multiplication in Karatsuba multiplication
KARATSUBA_LIMIT = 16


class MultiplicationSorting(QuantumCircuit):
    """
    A circuit that applies the operation I, x,y -> I, y + H_I x, x.
    This class is also responsible for the representation of I, using two functions:
    - "I_size" that specifies its size (in bits) from n bits
    - "random_I" that outputs a random I fitting this description, as well as its
        simple representation as an n-bit list

    This is the "sorting-based" implementation, where we use a switching network.
    Actually, there are two possible implementations in this setting:
    - basic: the internal constant matrix multiplication is performed naively
    - Karatsuba: the internal constant matrix multiplication, if the matrix is
    circulant, is performed with Karatsuba multiplication. 

    These correspond respectively to the flags "SORTING_BASIC" and "SORTING_KARATSUBA".

    Layout: I - x - y - ancilla
    """

    def random_I(n, k):
        return sorting_based_random_cols(n, k)

    def I_size(n):
        if type(n) == int:
            return len(batcher_switch_sequence(n))
        else:
            # upper bound
            logn = abstract_ceil(abstract_log2(n))
            # (n//4 * logn * (logn-1) + n - 1 if type(n) is int else
            return (n/4 * logn * (logn-1) + n - 1)

    def all_counts(n, k, do_swap=True, abstract=False):
        if abstract:
            return Symbol("D"), Symbol("Q"), GateCounts(cx=Symbol("CX"), ccx=Symbol("CCX"), x=Symbol("X"))
        log2n = abstract_ceil(abstract_log2(n))
        d_cp, _, g_cp = ControlledPermutation.all_counts(n)

        if MULTIPLICATION_FLAG == SORTING_BASIC:
            d_mp, q_mp, g_mp = ConstantMatrixVectorProduct.all_counts(n, k)
        elif MULTIPLICATION_FLAG == SORTING_KARATSUBA:
            # Karatsuba multiplication can be used for circulant matrices (BIKE
            # and HQC). In that case we expect to have two circulant blocks
            # and to use two multiplications.
            # if k != n//2:
            #    raise ValueError("Unsupported")
            d_mp, q_mp, g_mp = KaratsubaConstantCirculantMatrixVectorProduct.all_counts(
                k, limit=KARATSUBA_LIMIT)
            g_mp = g_mp*2
            d_mp = d_mp*2
        else:
            raise Exception("This shouldn't happen")

        # controlled_perm and its inverse + matrix product
        gcs = g_mp
        gcs += g_cp * 2

        depth = d_mp + 2*d_cp
        q = q_mp + MultiplicationSorting.I_size(n) + (3*log2n*n)
        # the 3*log n * n factor is not counted in our circuits, but it has to be
        if do_swap:
            gcs['swap'] = n-k

        return depth, q, gcs

    def ancilla_count(n, k, abstract=False):
        if abstract:
            return Symbol("A")
        return k

    def __init__(self, H, do_swap=True):
        """Build the circuit.

        Parameters:
            H -- matrix of dimension n * n-k (as a list of lists, H[i][j] is the element (i,j))
            do_swap -- True if the x and y registers are swapped after the operation.
                    Otherwise y is modified by the registers are not swapped.
        """
        super().__init__(name="multiplication")

        # retrieve dimensions of matrix
        self.n = len(H[0])
        self.k = self.n-len(H)
        self.H = H
        self.do_swap = do_swap

        # registers
        x_reg = QuantumRegister(self.n-self.k)
        y_reg = QuantumRegister(self.n-self.k)
        I_reg = QuantumRegister(MultiplicationSorting.I_size(self.n))

        controlled_perm = ControlledPermutation(self.n)

        x_completion = QuantumRegister(self.k)
        # complete x into n-bit register
        full_x_reg = x_reg[:] + x_completion[:]

        self.add_register(I_reg, x_reg, y_reg, x_completion)

        # compute v_reg by in-place permutation of full_x_reg
        self.append(controlled_perm, I_reg[:] + full_x_reg[:])

        if MULTIPLICATION_FLAG == SORTING_BASIC:
            self.append(ConstantMatrixVectorProduct(H),
                        full_x_reg[:] + y_reg[:])
            self.ancilla_nbr = self.k
        elif MULTIPLICATION_FLAG == SORTING_KARATSUBA:
            # case of block-circulant matrix
            if self.n % 2 == 1 or self.n // self.k != 2:
                raise ValueError("Not supported")
            col1 = [t[0] for t in H]
            col2 = [t[self.k] for t in H]

            karatsuba_ancilla_reg = QuantumRegister(
                KaratsubaConstantCirculantMatrixVectorProduct.ancilla_count(self.k, KARATSUBA_LIMIT))
            self.ancilla_nbr = self.k + len(karatsuba_ancilla_reg)
            self.add_register(karatsuba_ancilla_reg)
            self.append(KaratsubaConstantCirculantMatrixVectorProduct(
                col1, limit=KARATSUBA_LIMIT), full_x_reg[:self.k] + y_reg[:] + karatsuba_ancilla_reg[:])
            self.append(KaratsubaConstantCirculantMatrixVectorProduct(
                col2, limit=KARATSUBA_LIMIT), full_x_reg[self.k:] + y_reg[:] + karatsuba_ancilla_reg[:])
        else:
            raise ValueError("Not supported")
        # restore x_reg
        self.append(controlled_perm.inverse(), I_reg[:] + full_x_reg[:])

        if self.do_swap:
            # then swap x and y
            for i in range(self.n-self.k):
                self.swap(x_reg[i], y_reg[i])

    def test(self):
        for _ in range(20):
            I_val, columns = MultiplicationSorting.random_I(self.n, self.k)

            x = [randrange(2) for _ in range(self.n-self.k)]
            y = [randrange(2) for _ in range(self.n-self.k)]

            inp = I_val + x + y + [0]*(self.ancilla_nbr)
            out = simulate(self, inp)
            a, b = multiplication(self.H, columns, x[:], y[:])
            expected_output = I_val + a + b + [0]*(self.ancilla_nbr)

            assert out == expected_output


# ===========================
# choice of multiplication circuit

MultiplicationCircuit = MultiplicationSorting


def set_multiplication_flag(flag):
    global MultiplicationCircuit
    global MULTIPLICATION_FLAG
    if flag not in [SPACE_OPTIMIZED, SORTING_BASIC, SORTING_KARATSUBA]:
        raise ValueError("Wrong flag")
    MULTIPLICATION_FLAG = flag
    if flag == SPACE_OPTIMIZED:
        MultiplicationCircuit = MultiplicationBasic
    else:
        MultiplicationCircuit = MultiplicationSorting


def get_multiplication_circuit():
    global MultiplicationCircuit
    return MultiplicationCircuit


def get_multiplication_flag():
    global MULTIPLICATION_FLAG
    return MULTIPLICATION_FLAG
