# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""Implementation of a matrix-vector product when the matrix is binary, circulant
and constant (i.e. a linear circuit). Internally, we use a space-efficient 
reversible Karatsuba multiplication of binary polynomials which comes from
Gidney ("Asymptotically efficient quantum Karatsuba multiplication", 
arXiv:1904.07356, 2019).

"""

from qiskit import QuantumCircuit, QuantumRegister
from random import randrange

from .quantum_util.util import GateCounts, simulate
from .abstract_functions import abstract_ceil, abstract_log2



def pol_mult(p1, p2):
    """Multiplication of polynomials defined by binary lists (for testing)."""
    res = [0] * (len(p1) + len(p2) - 1)
    for i1, b1 in enumerate(p1):
        for i2, b2 in enumerate(p2):
            res[i1 + i2] ^= b1 * b2
    return res


class KaratsubaMultiplier(QuantumCircuit):
    """Multiplies an input polynomial by a constant polynomial. This is a
    completely linear circuit, made only of CNOTs.
    
    A parameter 'limit' specifies at which point we start using schoolbook multiplication.
    Typically limit = 16 will optimize the gate count and limit = 32 or 64 will
    be a better choice for the depth.
    """

    def all_counts(n, limit=64):
        """Counts are approximate here. n is approximated to a power of 2."""
        gcs = GateCounts()
        log2l = abstract_ceil(abstract_log2(limit))
        log2n = abstract_ceil(abstract_log2(n))
        ktmp = log2n - log2l
        gcs['cx'] = 3**(ktmp) * (limit**2 + 12)
        d = 3**(ktmp) * (limit + 3)
        q = 3 * n  # full qubit count
        return d, q, gcs

    def __init__(self, p, limit=64):
        """Build the circuit.
        
        Args:
            p -- constant polynomial (as a list of bits)
            limit -- bit-size below which we use schoolbook multiplication.
        """
        super().__init__()
        self.p = p

        # if the polynomial is small enough: use schoolbook multiplication
        d = len(p)  # degree is d-1
        self.d = d
        if self.d <= limit:
            inp_reg = QuantumRegister(d)
            out_reg = QuantumRegister(2 * d)

            self.add_register(inp_reg, out_reg)

            for i, b in enumerate(self.p):
                if b == 1:
                    for j in range(self.d):
                        self.cx(inp_reg[j], out_reg[j + i])
            return

        # otherwise: recurse
        # (A_1 + A_2 X^n) (P_1 + P_2 X^n)
        # = A_1 P_1 (1 + X^n) + X^n A_2 P_2 (1+ X^n) + X^n ((A_1 + A_2)(P_1 + P_2))

        assert self.d % 2 == 0  # otherwise not supported yet
        dp = d // 2
        inp_reg = QuantumRegister(d)
        out_reg = QuantumRegister(2 * d)
        self.add_register(inp_reg, out_reg)

        p1 = self.p[:dp]
        p2 = self.p[dp:]
        psum = [p1[i] ^ p2[i] for i in range(dp)]

        # multiply register by inverse of 1 + X^(n)
        for i in range(d):
            self.cx(out_reg[i], out_reg[i + dp])

        # + A_1 P_1 (1 + X^n) : first + A_1 P_1, then * (1 + X^n)
        self.append(KaratsubaMultiplier(p1, limit=limit),
                    inp_reg[:dp] + out_reg[:d])

        for i in reversed(range(d)):
            self.cx(out_reg[i], out_reg[i + dp])

        for i in range(d):
            self.cx(out_reg[i + dp], out_reg[i + 2 * dp])

        # + X^n A_2 P_2 (1+ X^n): same principle
        self.append(KaratsubaMultiplier(p2, limit=limit),
                    inp_reg[dp:] + out_reg[dp:3 * dp])

        for i in reversed(range(d)):
            self.cx(out_reg[i + dp],
                    out_reg[i + 2 * dp])  # goes up to maximal degree here

        # xor A_1 on A_2
        for i in range(dp):
            self.cx(inp_reg[i], inp_reg[i + dp])

        self.append(KaratsubaMultiplier(psum, limit=limit),
                    inp_reg[dp:] + out_reg[dp:3 * dp])

        # xor A_1 on A_2 again
        for i in range(dp):
            self.cx(inp_reg[i], inp_reg[i + dp])

    def test(self):
        for _ in range(10):
            input_pol = [randrange(2) for _ in range(self.d)]
            input_bits = input_pol + [0] * (2 * self.d)
            expected_output = input_pol + pol_mult(input_pol, self.p) + [0]
            output_bits = simulate(self, input_bits)
            #print(expected_output)
            #print(output_bits)
            assert expected_output == output_bits


class ConstantCirculantMatrixVectorProduct(QuantumCircuit):
    """
    Naive circuit for circulant matrix-vector multiplication, in O(n^2) CNOT gates.
    
    A circulant matrix is represented by its first *column*. Subsequent columns
    are shifted down by one bit.
    """

    def all_counts(n):
        gcs = GateCounts(cx=n**2)
        return n, 2 * n, gcs

    def __init__(self, C):
        super().__init__(name="circulant_matrix_product")
        self.n = len(C)
        self.C = C

        # registers
        x_reg = QuantumRegister(self.n)
        y_reg = QuantumRegister(self.n)

        self.add_register(x_reg, y_reg)

        for i in range(self.n):
            # all of the operations under this loop are done in depth 1, since
            # all the (a,b) pairs are distinct
            for u in range(self.n):
                a, b = u, (i + u) % self.n
                if C[(-b + a) % self.n] == 1:
                    self.cx(x_reg[b], y_reg[a])


class KaratsubaConstantCirculantMatrixVectorProduct(QuantumCircuit):
    """Circuit for circulant matrix-vector product that uses Karatsuba
    multiplication of polynomials. 

    Layout:
    input register - output register - ancilla
    """

    def all_counts(n, limit=64):
        """Counts are approximate here. n is approximated to a power of 2."""
        gcs = GateCounts(cx=2 * abstract_ceil(abstract_log2(n)))
        d1, _, g1 = KaratsubaMultiplier.all_counts(n, limit=limit)
        gcs += g1 * 2
        d = 2 * d1 + 1
        # if n is not an int, we're running in 'asymptotic' mode and we simplify
        n2 = 2**(abstract_ceil(abstract_log2(n / limit))) * limit if type(n) == int else n
        q = n + (n2-n) + 2*n2 + n

        return d, q, gcs

    def ancilla_count(n, limit=64):
        n2 = 2**(abstract_ceil(abstract_log2(n / limit))) * limit
        return (n2-n) + 2*n2

    def __init__(self, C, limit=64):
        super().__init__(name="circulant_matrix_product")
        self.n = len(C)
        self.C = C

        if self.n < limit:
            raise ValueError("Currently unsupported")

        # approximate n by 2**t * limit
        n2 = 2**(abstract_ceil(abstract_log2(self.n / limit))) * limit
        padded_C = C + [0] * (n2 - self.n)

        # consider input as n2-bit polynomial which is multiplied by C
        # (another n2-bit polynomial), then reduce mod x^n + 1

        # registers
        x_reg = QuantumRegister(self.n)
        x_padding = QuantumRegister(n2 - self.n)

        tmp_y_reg = QuantumRegister(2 * n2)
        y_reg = QuantumRegister(self.n)

        self.add_register(x_reg, y_reg, x_padding, tmp_y_reg)

        k_mul = KaratsubaMultiplier(padded_C, limit=limit)
        self.append(k_mul, x_reg[:] + x_padding[:] + tmp_y_reg[:])

        # reduce mod x^n + 1
        for i in range(2 * n2):
            self.cx(tmp_y_reg[i], y_reg[i % self.n])

        self.append(k_mul.inverse(), x_reg[:] + x_padding[:] + tmp_y_reg[:])


    def test(self):
        # check correctness against the naive circuit
        qc1 = self
        qc2 = ConstantCirculantMatrixVectorProduct(self.C)
        n = self.n
        for _ in range(10):
            input_bits = [randrange(2) for _ in range(n)]
            input_qc1 = input_bits + [0] * (len(qc1.qubits) - n)
            input_qc2 = input_bits + [0] * (len(qc2.qubits) - n)
            out_1 = simulate(self, input_qc1)[n:(2 * n)]
            out_2 = simulate(qc2, input_qc2)[n:(2 * n)]
            assert out_1 == out_2


