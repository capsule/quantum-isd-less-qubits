# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Simple script to provide cost estimates for the Wiedemann inversion circuit,
under various parameters of code-based KEMs in the Round 4 of the NIST
post-quantum standardization, compared with those of Table 3 in 'Improving the 
Efficiency of Quantum Circuits for Information Set Decoding' (Perriello, 
Barenghi, Pelosi, ACM Trans. Quant. Comp. 2023).

Unfortunately as the circuits become quite large, we have to rely on analytic
estimates (obtained via the class methods .all_counts() of our classes). 
However these estimates are quite precise: apart from some subcircuits which
have been neglected, they are computed in an exact way.

There are three arguments to provide when running this file as a script:

* --table : a choice between 'asymptotic', 'previous' and 'new'. Default is 'new'.
    * 'asymptotic' will print a (large) table comparing the costs, computed with
    .all_counts(), with the simplified asymptotic formulas given in the paper
    * 'previous' will output a table of circuit costs taken from Table 3 in 'Improving the 
    Efficiency of Quantum Circuits for Information Set Decoding' (Perriello, 
    Barenghi, Pelosi, ACM Trans. Quant. Comp. 2023).
    * 'new' will print our results (obtained with .all_counts())
    
    'previous' and 'new' with different multiplication circuits will correspond
    to the entries of Table 1.
    
* --prec : choice of precision (number of digits after comma) for the displayed numbers,
    which are in log_2. Default is 1.

* --mult : choice of multiplication circuit. Default is 'spaceopt'
    * 'spaceopt' will use the space-optimized circuit (Section 5.1)
    * 'toffoliopt' will use the Toffoli-optimized circuit (Section 5.2)
    * 'karatsuba' will use Karatsuba-based multiplication for circulant matrices (Section 5.3)

In order to obtain all sub-sections of Table 1 in order, run:
    python3 estimations.py --table previous
    python3 estimations.py --table new --mult spaceopt
    python3 estimations.py --table new --mult toffoliopt
    python3 estimations.py --table new --mult karatsuba

This may take some time, but should not take more than a few minutes.
"""

from qiskit import QuantumCircuit, QuantumRegister

import tabulate
import argparse

from compressed_prange.inversion_circuit import WiedemannInversion
from compressed_prange.multiplication import (SPACE_OPTIMIZED, SORTING_BASIC, SORTING_KARATSUBA,
                                              set_multiplication_flag, get_multiplication_flag)

from math import log, ceil


def print_line_as_latex(l):
    """
    Print a list of arguments as a line in a LateX table (separated by & and
    ending with double backslash).
    """
    print(*l, end='', sep=' & ')
    print("\\\\")


def table_params(params, flag="previous", precision=0, as_latex=False):
    """Estimating counts for parameters of code-based cryptosystems.

    Parameters:
        params -- series of parameters for the benchmarks
        flag -- "previous" if we only print the previous counts
                "asymptotic" if we compare the asymptotic and non-asymptotic formulas
                "comparison" if we compare ours with the previous counts
                otherwise we just print ours
        precision -- precision of the numbers to be displayed. If None, will round
                upwards using the 'ceil' function. Otherwise, should be an integer.
                i represents a precision of 10^(-i).
        as_latex -- if True, print lines for a lateX table in the terminal. 
                Otherwise, print a readable table. 
    """
    func = (lambda x: str(ceil(x))) if precision is None else (
        lambda x: "{:.{}f}".format(x, precision))

    if flag == "asymptotic":
        header = ["Scheme", "n", "k", "CCX", "Total gates", "Depth", "Qubits",
                  "Asymptotic CCX", "Asymptotic gates", "Asymptotic depth", "Asymptotic qubits"]
    else:
        header = ["Scheme", "n", "k", "CCX",
                  "Total gates", "Depth", "Qubits", "DW"]
    table = [header]

    ctr = 0
    for key in params:
        ctr += 1
        n, k, prev_gates, prev_depth, prev_qubits = params[key]
        depth, qubits, gates = WiedemannInversion.all_counts(n, k)

        if get_multiplication_flag() == SPACE_OPTIMIZED:
            depth_asymptotic = 96 * n * (n - k) * log(n - k, 2)
            qubits_asymptotic = 18 + n + 19 * (n - k)
            ccx_asymptotic = 120 * n * (n - k)**2
            gates_asymptotic = 336 * n * (n - k)**2
        elif get_multiplication_flag() == SORTING_BASIC:
            depth_asymptotic = 24*n*(n-k)
            qubits_asymptotic = (1 / 4 * n * (log(n, 2) - 1) *
                                 (log(n, 2)) + 19 * (n - k) + n + 17 + 3*n*log(n, 2))
            qubits_asymptotic = (1/4*n*(log(n, 2) - 1)*log(n, 2) - 9*k +
                                 10*n + max(-2*k + 3*n, -2*k + 3*n + 2, -10*k + 10*n + 11) + 6)
            qubits_asymptotic = (1/4*n*(ceil(log(n, 2)) - 1)*ceil(log(n, 2)) + 3*n*ceil(
                log(n, 2)) - 9*k + 10*n + max(-2*k + 3*n, -2*k + 3*n + 2, -10*k + 10*n + 11) + 6)
            ccx_asymptotic = (12 * (n - k) * n * (log(n, 2))**2 + 116*(n-k)**2)
            gates_asymptotic = (24 * n * (n - k)**2 + (356+116)
                                * (n-k)**2 + 12 * (n - k) * n * (log(n, 2))**2)
        else:
            # unsupported
            depth_asymptotic = None
            qubits_asymptotic = None
            ccx_asymptotic = None
            gates_asymptotic = None

        if flag == "asymptotic":
            # compare with asymptotic formulas
            l = [key, n, k, func(log(gates["ccx"], 2)),
                 func(log(sum([gates[v] for v in gates]), 2)),
                 func(log(depth, 2)), func(log(qubits, 2)),
                 func(log(ccx_asymptotic, 2)),
                 func(log(gates_asymptotic, 2)),
                 func(log(depth_asymptotic, 2)),
                 func(log(qubits_asymptotic, 2))]
        elif flag == "previous":
            l = [key, n, k, '', prev_gates, prev_depth,
                 prev_qubits, prev_depth+prev_qubits]
        else:
            l = [key, n, k, func(log(gates["ccx"], 2)),
                 func(log(sum([gates[v] for v in gates]), 2)),
                 func(log(depth, 2)), func(log(qubits, 2)),
                 func(log(depth, 2) + log(qubits, 2))]
        if as_latex:
            print_line_as_latex(l)
        else:
            # print("In progress... ", ctr, "/", max_ctr)
            table.append(l)
    if not as_latex:
        # print the table
        print(tabulate.tabulate(
            table[1:],  headers=table[0], tablefmt='fancy_grid', disable_numparse=True))


_HELP_TABLE = {
    "asymptotic": """
Table of comparisons between the exact cost formulas and the 
asymptotic cost formulas, as given in the paper, which keep only the higher-order
terms.
""",
    "previous": """
Table of circuit costs taken from Table 3 in 'Improving the 
Efficiency of Quantum Circuits for Information Set Decoding' (Perriello, 
Barenghi, Pelosi, ACM Trans. Quant. Comp. 2023).
""",
    "new": """
Table of circuit costs for our new circuits.
"""
}

_HELP_MULT = {
    "spaceopt": """We are using the space-optimized matrix multiplication circuit,
which represents the selection of columns using an n-bit vector.
""",
    "toffoliopt": """We are using the Toffoli-optimized matrix multiplication circuit,
which represents the selection of columns using a switching network.
""",
    "karatsuba": """We are using the Toffoli-optimized matrix multiplication circuit,
with Karatsuba-based matrix multiplication (only concerns block-circulant matrices).
"""
}


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog="estimations", description="Displays cost estimates for the matrix inversion circuits designed in the paper, in log2 (all entries of Table 1).")
    parser.add_argument("--table",  help="Table selection",
                        default="new", choices=['asymptotic', 'previous', 'new'])
    parser.add_argument("--prec", type=int, default=1,
                        help="Precision in the counts (default = 1 digit)")
    parser.add_argument("--mult", type=str, default="spaceopt",
                        help="Choice of matrix multiplication technique", choices=["spaceopt", "toffoliopt", "karatsuba"])

    args = parser.parse_args()

    print(_HELP_TABLE[args.table])
    print(_HELP_MULT[args.mult])

    if args.mult == "spaceopt":
        set_multiplication_flag(SPACE_OPTIMIZED)
    elif args.mult == "toffoliopt":
        set_multiplication_flag(SORTING_BASIC)
    else:
        set_multiplication_flag(SORTING_KARATSUBA)

    if get_multiplication_flag() == SORTING_KARATSUBA:
        # only applicable to BIKE and HQC
        params = {
            # n, k, gates, depth, qubits
            "BIKE L1": (24646, 12323, 108 - 65, 93 - 65, 29),
            "BIKE L3": (49318, 24659, 142 - 96, 127 - 96, 31),
            "BIKE L5": (81946, 40973, 178 - 130, 162 - 130, 33),
            "HQC L1": (35338, 17669, 104 - 59, 89 - 59, 30),
            "HQC L3": (71702, 35851, 140 - 93, 125 - 93, 32),
            "HQC L5": (115274, 57637, 173 - 123, 157 - 123, 34)
        }
    else:
        params = {
            # n, k, gates, depth, qubits
            "BIKE L1": (24646, 12323, 108 - 65, 93 - 65, 29),
            "BIKE L3": (49318, 24659, 142 - 96, 127 - 96, 31),
            "BIKE L5": (81946, 40973, 178 - 130, 162 - 130, 33),
            "HQC L1": (35338, 17669, 104 - 59, 89 - 59, 30),
            "HQC L3": (71702, 35851, 140 - 93, 125 - 93, 32),
            "HQC L5": (115274, 57637, 173 - 123, 157 - 123, 34),
            "McEliece L1": (3488, 2720, 102 - 72, 92 - 72, 22),
            "McEliece L3": (4608, 3360, 125 - 93, 115 - 93, 23),
            "McEliece L5-1": (6688, 5024, 165 - 131, 154 - 131, 24),
            "McEliece L5-2": (6960, 5413, 165 - 132, 155 - 132, 24),
            "McEliece L5-3": (8192, 6528, 184 - 150, 173 - 150, 24)
        }

    if args.table == "asymptotic" and args.mult == "karatsuba":
        raise ValueError("Unsupported parameters")

    table_params(params, flag=args.table, precision=args.prec, as_latex=False)
