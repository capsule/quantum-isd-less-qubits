# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
This script runs all the unit tests in our circuits. Each test will:
- check on one or a few random inputs that the circuit runs correctly
- check that the cost estimates output by .all_counts (theoretical formula)
match or are below the actual cost (gate count, depth and qubits).

Simply run:

python3 test_circuits.py

Tests are enforced by 'assert'. If the test is not passed, there will be an error.
"""

import random

from compressed_prange.inversion_circuit import SequenceEvaluation, PolynomialEvaluation, WiedemannInversion
from compressed_prange.multiplication import SPACE_OPTIMIZED, SORTING_BASIC, SORTING_KARATSUBA
from compressed_prange.multiplication import set_multiplication_flag, get_multiplication_circuit
from compressed_prange.karatsuba_multiplication import KaratsubaConstantCirculantMatrixVectorProduct, KaratsubaMultiplier
from compressed_prange.sorting import Switch, BatcherSort, IntegerComparator
from compressed_prange.berlekamp_massey import ControlledNegateReverse, BerlekampMassey
from compressed_prange.list_operations import Fanin, Fanout, ControlledShiftLeft, ListReverter
from compressed_prange.quantum_util.util import full_decompose, gate_counts



def test_circuit(the_class, class_args, all_counts_args=None, verb=True):
    """Check for a given class and instance that the actual gate, depth
    and qubit counts are lower than those estimated by the formulas. This is
    done via assert.

    Args:
        the_class -- class name
        class_args -- arguments to initialize the class. The class must be
            a QuantumCircuit subclass with a static method .all_counts and
            a method test.
        all_counts_args -- arguments to compute the corresponding 'all_counts' method
            (not necessarily the same, but default is None and will put the same arguments)
        verb -- verbose
    """
    if verb:
        print("Checking an instance of ", the_class)
    qc = the_class(*class_args)
    qc.test()
    if verb:
        print("[PASSED] Circuit runs correctly.")
    qc = full_decompose(qc)
    d = gate_counts(qc)
    if verb:
        print("Circuit counts (depth, qubits, gates):",
            qc.depth(), len(qc.qubits), d)
    depth, qubits, gates = the_class.all_counts(
        *(class_args if all_counts_args is None else all_counts_args))
    if verb:
        print("Theoretical counts (depth, qubits, gates):", depth, qubits, gates)
    assert depth >= qc.depth()
    assert qubits >= len(qc.qubits)
    assert gates >= d
    if verb:
        print("[PASSED] Practical counts are below theoretical formulas")


# ================ list_operations module

# fan-in circuit
test_circuit(Fanin, class_args=[50])
# fan-out circuit
test_circuit(Fanout, class_args=[50])
# controlled shift left
test_circuit(ControlledShiftLeft, class_args=[100, 7], all_counts_args=[100])

# formula for ListReverter might be valid only for large values of n
test_circuit(ListReverter, class_args=[100])

# ================== Berlekamp-Massey module

test_circuit(ControlledNegateReverse, class_args=[100, 50])

test_circuit(BerlekampMassey, class_args=[100, 51])


# ================= sorting module

test_circuit(IntegerComparator, class_args=[10])

test_circuit(Switch, class_args=[10])

# formula is not tight when n is not a power of 2 (but still works)
test_circuit(BatcherSort, class_args=[34, 5])
test_circuit(BatcherSort, class_args=[64, 5])


# ================= Karatsuba matrix multiplication subroutines

C = [random.randrange(2) for _ in range(40)]
test_circuit(KaratsubaConstantCirculantMatrixVectorProduct,
            class_args=[C, 16], all_counts_args=[40, 16])

test_circuit(KaratsubaMultiplier, class_args=[C, 16], all_counts_args=[40, 16])


# ================== matrix multiplication module

# set to first multiplication circuit (space-optimized)
set_multiplication_flag(SPACE_OPTIMIZED)

n, k = 16, 5
H = [[random.randrange(2) for _ in range(n)] for _ in range(n-k)]
test_circuit(get_multiplication_circuit(),
            class_args=[H], all_counts_args=[n, k])

# set to second multiplication circuit (Toffoli-optimized)
set_multiplication_flag(SORTING_BASIC)

test_circuit(get_multiplication_circuit(),
            class_args=[H], all_counts_args=[n, k])

# test the case of Karatsuba-based multiplication for double-block circulant matrix
# (gate-optimized)
set_multiplication_flag(SORTING_KARATSUBA)

# create double block-circulant matrix
k = 18
n = 2*k
col1 = [random.randrange(2) for _ in range(k)]
col2 = [random.randrange(2) for _ in range(k)]
H = [[col1[(i-j) % k] if i < k else col2[(i-j) % k]
      for i in range(2*k)] for j in range(k)]

test_circuit(get_multiplication_circuit(),
             class_args=[H], all_counts_args=[n, k])


# =================== Wiedemann inversion module


# test with first matrix product
set_multiplication_flag(SPACE_OPTIMIZED)

n, k = 10, 5
m = 10
H = [[random.randrange(2) for _ in range(n)] for _ in range(n - k)]
test_circuit(SequenceEvaluation, class_args=[
             H, 0, m], all_counts_args=[n, k, m])

test_circuit(PolynomialEvaluation, class_args=[
             H, m], all_counts_args=[n, k, m])


s = [random.randrange(2) for _ in range(n - k)]
test_circuit(WiedemannInversion, class_args=[H, s], all_counts_args=[n, k])

# test with second matrix product
set_multiplication_flag(SORTING_BASIC)
test_circuit(SequenceEvaluation, class_args=[
             H, 0, m], all_counts_args=[n, k, m])

test_circuit(PolynomialEvaluation, class_args=[
             H, m], all_counts_args=[n, k, m])

test_circuit(WiedemannInversion, class_args=[H, s], all_counts_args=[n, k])
