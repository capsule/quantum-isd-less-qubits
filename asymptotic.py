# Copyright 2024 Clémence Chevignard, Pierre-Alain Fouque, André Schrottenloher

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Permission is hereby granted, free of charge, to any person obtaining a copy 
# of this software and associated documentation files (the “Software”), to deal 
# in the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
This script provides asymptotic estimates of the costs of the inversion circuit. Run:

python3 asymptotic.py mult --simplify

where 'mult' should be 'spaceopt' or 'toffoliopt' and denotes the multiplication
circuit that is used internally.

Karatsuba-based multiplication is not supported at the moment. The resulting 
formulas would be too complicated to simplify automatically, because of the
constant parameter 'limit' used in the KaratsubaMultiplier class.

With the option --simplify, the formulas in output will be simplified by keeping
only the higher-order terms. They may differ from those of the paper because we
have sometimes kept more terms in the latter. Without this option, the formulas
will not be simplified, but this makes them very difficult to read.

Internally, this script works by replacing the inputs in .all_counts() functions
(such as n and k) by variables. Similarly functions used in .all_counts() such as
log, ceil and floor are replaced by abstract functions. Therefore instead of
computing numbers, .all_counts() is used here to compute formulas. 


As the exact formula becomes quickly unreadable, we have implemented an ad hoc function to
keep only the higher-order terms, assuming that n and k have the same order.

"""


from sympy import Mul, Symbol, Wild
from compressed_prange.inversion_circuit import WiedemannInversion
from compressed_prange.multiplication import SPACE_OPTIMIZED, SORTING_BASIC
from compressed_prange.multiplication import set_multiplication_flag
from compressed_prange.abstract_functions import *

import argparse



_HELP_MULT = {
    "spaceopt": """We are using the space-optimized matrix multiplication circuit,
which represents the selection of columns using an n-bit vector.
""",
    "toffoliopt": """We are using the Toffoli-optimized matrix multiplication circuit,
which represents the selection of columns using a switching network.
""",
    "karatsuba": """We are using the Toffoli-optimized matrix multiplication circuit,
with Karatsuba-based matrix multiplication (only concerns block-circulant matrices).
"""
}


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog="asymptotic", description="Displays asymptotic formulas for the costs of the inversion circuit.")
    parser.add_argument("mult", type=str, default="spaceopt",
                        help="Choice of matrix multiplication technique", choices=["spaceopt", "toffoliopt", "karatsuba"])
    parser.add_argument("--simplify", action='store_true',
                        help="Simplify the formulas by keeping the highest-order term")

    args = parser.parse_args()

    print(_HELP_MULT[args.mult])

    if args.simplify:
        print("The formulas are simplified")
    else:
        print("The formulas are not simplified")

    if args.mult == "spaceopt":
        set_multiplication_flag(SPACE_OPTIMIZED)
    elif args.mult == "toffoliopt":
        set_multiplication_flag(SORTING_BASIC)
    elif args.mult == "karatsuba":
        raise ValueError("Karatsuba-based multiplication is unsupported")

    n = Symbol('n')
    k = Symbol('k')
    m = Symbol('m')

    kp = Symbol('kp')

    depth, qubits, gates = (
        WiedemannInversion.all_counts(n, k, abstract=False))

    def myfirst(*x):
        """A replacement function for 'min': take the first argument."""
        return x[0]

    def transform_expression(e):
        """
        Simplify an expression by keeping only the higher-order terms. Do not use
        this function for general expressions, we tested it only for cost estimates
        in our circuits. We assume that n and k are asymptotically equivalent.
        """
        if not args.simplify:
            return e
        tmp = e
        a = Wild('a')

        # remove 'ceil' where it appears
        tmp = tmp.replace(abstract_ceil(a), a)
        # remove 'floor' where it appears
        tmp = tmp.replace(abstract_floor(a), a)
        # transform a 'min' by taking the first argument only
        tmp = tmp.replace(abstract_min, myfirst)
        # transform a 'max' by taking the first argument only
        tmp = tmp.replace(abstract_max, myfirst)
        # replace k by n-k' (this makes the formulas more natural, since
        # the factor (n-k) appears more often than k)
        tmp = tmp.subs(k, n-kp)
        tmp = tmp.expand()

        ordered_term_list = []

        # order the terms in the expanded expression by counting how many
        # factors and log-factors they have
        for t in tmp.args:
            l = list(t.args) if t.func == Mul else [t]
            terms, log_terms = 0, 0
            for tt in l:
                # we expect tt to be: a number, a log(..)^p, a n^p or a single term
                split_term = str(tt).split("**")
                # determine the power
                pw = 1 if len(split_term) == 1 else int(split_term[1])
                # print(split_term, pw)
                if not tt.has(n) and not tt.has(k) and not tt.has(kp):
                    # constant
                    pass
                elif "abstract_log2" in split_term[0]:
                    # log term
                    log_terms += pw
                else:
                    # non-log term
                    terms += pw

            ordered_term_list.append((terms, log_terms, t))
        
        # sort first by number of non-log terms, then number of log terms
        ordered_term_list.sort(key=lambda x: (x[0], x[1]))
        ordered_term_list.reverse()
        #print(ordered_term_list)

        # sum all the terms which have the same asymptotic
        max_a, max_b = ordered_term_list[0][0], ordered_term_list[0][1]
        new_expr = 0
        for a, b, t in ordered_term_list:
            if a == max_a and b == max_b:
                new_expr += t
        # replace kp by n-k
        new_expr = new_expr.subs(kp, n-k)
        # return the simplified expression
        return new_expr



    print("Depth:")
    print(transform_expression(depth))

    print("Qubits:")
    print(transform_expression(qubits))

    print("CCX Gates:")
    print(transform_expression(gates['ccx']))

    print("Gates:")
    print(transform_expression(gates['ccx'] + gates['x'] + gates['cx']))

    print("CX Gates:")
    print(transform_expression(gates['cx']))

    print("X Gates:")
    print(transform_expression(gates['x']))
